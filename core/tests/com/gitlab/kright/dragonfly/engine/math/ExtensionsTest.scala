/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.math

import com.badlogic.gdx.math.{Quaternion, Vector2, Vector3}
import org.scalatest.funsuite.AnyFunSuite

class ExtensionsTest extends AnyFunSuite {

  test("testQuaternionExt") {
    val q = new Quaternion()

    assert(q.getX() == new Vector3(1, 0, 0))
    assert(q.getY() == new Vector3(0, 1, 0))
    assert(q.getZ() == new Vector3(0, 0, 1))
    assert(q.rotAngle == 0f)
  }

  test("testVector2Ext") {
    val v = new Vector2(1, 2)
    val v2 = new Vector2()
    v2 := (1, 2)
    assert(v == v2)
  }

  test("testVector3Ext") {
    val v = new Vector3(1, 2, 3)
    val v2 = new Vector3()
    v2 := (1, 2, 3)
    assert(v == v2)
  }
}
