/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

import com.gitlab.kright.dragonfly.engine.math.{Derivative, Position}

/**
  * visible parameters of airplane
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait VisibleState extends TimeStamp {

  def position: Position

  def speed: Derivative
}
