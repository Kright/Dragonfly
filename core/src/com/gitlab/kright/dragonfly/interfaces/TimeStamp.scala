/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

/**
  * for example, airplane state or player input
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait TimeStamp {

  def time: Long
}
