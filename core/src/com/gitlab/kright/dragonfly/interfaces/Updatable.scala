/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

/**
  * something, which needs to be updated every tick
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait Updatable {

  //todo add FPSCounter
  def update(): Unit
}
