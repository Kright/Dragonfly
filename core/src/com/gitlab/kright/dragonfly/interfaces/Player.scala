/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

/**
  *
  * Created by Igor Slobodskov on 09.07.2015.
  */
class Player(val nickName: String, val id: Long) extends UniqueId
