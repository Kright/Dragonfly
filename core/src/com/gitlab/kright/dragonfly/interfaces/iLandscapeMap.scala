/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

import com.badlogic.gdx.math.{Rectangle, Vector2, Vector3}

/**
  *
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait iLandscapeMap {

  def getHeightAndNormal(position: Vector2, result: Vector3, dx: Float = 1f): Float

  def getHeight(position: Vector2): Float

  def bounds: Rectangle
}
