/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.interfaces

/**
  * something with id
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait UniqueId {

  def id: Long
}
