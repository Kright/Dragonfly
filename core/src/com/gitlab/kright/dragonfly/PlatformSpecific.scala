/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly

import com.badlogic.gdx.math.Vector2

trait PlatformSpecific {
  def cursor: Vector2
}

object PlatformSpecific {
  var instance: PlatformSpecific = null
}