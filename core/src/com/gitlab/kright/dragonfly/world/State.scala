/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.interfaces.VisibleState

/**
  * airplane state
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class State(var time: Long) extends VisibleState {

  val position = new Position()

  val speed = new Derivative()

  def interpolate(next: State, t: Long): State = {
    if (t == time) return this
    if (t == next.time) return next
    if (next.time == this.time) return this

    val result = new State(t)
    val timeStep = (next.time - time).toFloat
    assert(timeStep != 0f)
    val k0 = (t - time) / timeStep
    val k1 = 1f - k0

    val sp = result.speed.linear
    sp.madd(speed.linear, k1)
    sp.madd(next.speed.linear, k0)

    result.position.pos := this.position.pos + (sp + speed.linear) * ((0.5f * 0.001f) * (t - time))
    val ds = (speed.linear + next.speed.linear) * (0.5f * 0.001f * timeStep)
    val dr = next.position.pos - position.pos
    dr -= ds
    result.position.pos.madd(dr, k0)

    result.speed.angular.madd(speed.angular, k1)
    result.speed.angular.madd(next.speed.angular, k0)

    result.position.rot.setLerp(position.rot, next.position.rot, k0)

    //I hope it works
    result
  }
}
