/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Quaternion, Vector3}
import com.gitlab.kright.dragonfly
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  *
  * Created by Igor Slobodskov on 17.07.2015.
  */
class AirplaneModelImpl extends Airplane {

  val mass: Float = 900
  // looks like i-153

  val I: Matrix4 = new Matrix4() {
    `val`(0) = mass * 5 * 5 / 12
    `val`(5) = mass * 5 * 5 / 12
    `val`(10) = mass * 0.5f / 12 + 0.1f * mass * 10 * 10 / 12
  }

  //aerodynamics
  val planesS: Float = 20f
  val planeSVert: Float = 6f
  //from left corner to right
  val planesL: Float = 10f
  val crossSectionalArea: Float = 0.4f
  val lambda: Float = planesL * planesL / planesS

  val maxAngle: Float = 0.4f
  val cDrag: Float = 0.4f

  val enginePower: Float = 300000 //watts

  val controlMoment: Vector3 = new Vector3(2f, 0.4f, 1f) // pitch, yaw and roll coefficients


  val rotMoment: Float = 3f

  val trimmerPitch: Float = 0.2f

  val stabilizationMoment: Vector3 = new Vector3(10f, 10f, 0f) // same

  private def cy(angle: Float): Float = {
    val pi = 3.1415928535f
    if (angle > maxAngle) (pi - angle) / (pi - maxAngle)
    else if (angle < -maxAngle) -(pi + angle) / (pi - maxAngle)
    else angle / maxAngle
  }

  private def liftingForce(speed2: Float, angle: Float, air_density: Float): Float = cy(angle) * air_density * speed2 * 0.5f * planesS

  private def dragForce(speed2: Float, air_density: Float): Float = 0.5f * air_density * speed2 * cDrag * crossSectionalArea

  private def indDragForce(speed2: Float, air_density: Float, liftForce: Float): Float =
    if (speed2 > 10f) {
      liftForce * liftForce / (3.14f * lambda * 0.5f * air_density * speed2 * planesS)
    } else 0f

  private def steerForce(speed2: Float, angle: Float, air_density: Float): Float = angle * air_density * speed2 * 0.5f * planeSVert


  /**
    * @param position   current position
    * @param speed      current speed
    * @param planeInput player input (engine, ailerons, etc)
    * @return Derivative - linear force in global coordinates and angular - in local
    */
  def getForceAndMoment(position: Position, speed: Derivative, planeInput: PlaneInput): Derivative = {
    this.getForceAndMoment(position.pos, speed.linear, position.rot, planeInput)
  }

  /*
  private val tempM = new Matrix4()
  private val tempQ = new QuaternionS()

  def getInertiaTensor(orientation: Quaternion): Matrix4 = {
    tempM.set(orientation)
    tempM.mul(I)
    tempQ := orientation
    tempQ.conjugate()
    tempM.rotate(tempQ)
    tempM
  }*/

  @deprecated("refactoring", since = "2015-12-12")
  def getForceAndMoment(position: Vector3, speed: Vector3, orientation: Quaternion, planeInput: PlaneInput): Derivative = {

    val drNorm = new Vector3(speed)
    if (speed.length > 1f)
      drNorm.normalize()

    val force = new Vector3()
    val up = orientation.getY()
    val forward = orientation.getZ() * -1
    val right = orientation.getX()
    val airDensity = dragonfly.world.WorldParams.atmosphere.airDensity(position)

    force += dragonfly.world.WorldParams.gVect * mass

    force -= drNorm * dragForce(speed.length2, airDensity)

    val speedF = speed dot forward
    val lifting = liftingForce(speedF * speedF, -Math.asin((up dot speed) / (0.1f + speed.length)).toFloat, airDensity)
    force += up * lifting

    //force -= drNorm * indDragForce(speedF * speedF, airDensity, lifting)

    val engineTrust = planeInput.engineTrust * enginePower / Math.max(70, speedF)
    force += forward * engineTrust

    force -= right * steerForce(speed.length2, Math.asin((right dot speed) / (0.1f + speed.length)).toFloat, airDensity)

    val moment = new Vector3()

    moment.x += airDensity * (planeInput.pitch + trimmerPitch) * controlMoment.x * speed.length2
    moment.y -= airDensity * planeInput.yaw * controlMoment.y * speed.length2
    moment.z -= airDensity * planeInput.roll * controlMoment.z * speed.length2

    moment.x += drNorm dot orientation.getY() * stabilizationMoment.x * speed.length2 * airDensity
    moment.y -= drNorm dot orientation.getX() * stabilizationMoment.y * speed.length2 * airDensity

    moment.z += drNorm dot orientation.getX() * lifting * 2f

    new Derivative(force, moment)

    //new Derivative(new Vector3f, new Vector3f(planeInput.pitch, -planeInput.yaw, -planeInput.roll) * 2000)
  }

  def getForceAndMoment(state: State, input: PlaneInput): Derivative = getForceAndMoment(state.position, state.speed, input)
}
