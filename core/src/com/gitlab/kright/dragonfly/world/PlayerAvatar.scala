/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.gitlab.kright.dragonfly.interfaces.{Player, VisibleState}
import com.gitlab.kright.dragonfly.playerio.{PlaneInput, PlaneNaiveInput}

import scala.collection.mutable.ArrayBuffer

/**
  * representation of player on his own device
  * so, input directly used here
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class PlayerAvatar(player: Player, model: Airplane, initialState: State)
  extends Avatar(player, model) {

  private val timeStep = 20
  private val stateGen = new StateGenerator

  // states count at least 2
  private val states = new ArrayBuffer[State]()
  states += initialState
  states += initialState

  private val planeInput = new PlaneNaiveInput(initialState.time)


  def nextState(now: State, timeStep: Long): State = {
    stateGen.nextState(now, timeStep, model, planeInput)
  }

  private def getStatesPair(time: Long): (State, State) = {
    for (i <- (states.length - 2) to (0, -1))
      if (states(i).time < time)
        return (states(i), states(i+1))

    (states(0), states(1))
  }

  override def getState(time: Long): VisibleState = {
    while (states.last.time < time) {
      states += nextState(states.last, timeStep)
    }
    val (prev, next) = getStatesPair(time)
    prev.interpolate(next, time)
  }

  def setInput(newInput: PlaneInput): Unit = {
    assert(newInput.time >= planeInput.time)

    this.planeInput := newInput

    for (i <- states.size - 1 until(0, -1)) {
      if (states(i).time <= planeInput.time) {
        return
      } else {
        states.remove(i)
      }
    }
  }
}
