/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.Vector3

/**
  * world physics params
  *
  * Created by Igor Slobodskov on 17.07.2015.
  */
object WorldParams {

  val g = 9.8f
  val gVect = new Vector3(0f, -g, 0f)

  val north = new Vector3(0f, 0f, 1f)
  val east = new Vector3(-1f, 0f, 0f)
  val zenith = new Vector3(0f, 1f, 0f)

  val atmosphere = new Atmosphere()

  val astronomy = new Astronomy(north, zenith, latitude = 56f)
}




