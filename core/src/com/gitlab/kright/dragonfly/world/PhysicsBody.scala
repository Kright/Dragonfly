/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.Matrix4

/**
  * Created by Igor Slobodskov on 30.08.2015.
  */
trait iPhysicsBody {

  def mass: Float

  def I: Matrix4
}

class PhysicsBody(val mass: Float, val I: Matrix4) extends iPhysicsBody {

  def this(m: Float, ix: Float, iy: Float, iz: Float) = this(m, new Matrix4(
    Array[Float](
      ix, 0, 0, 0,
      0, iy, 0, 0,
      0, 0, iz, 0,
      0, 0, 0, 1)))

  def this(m: Float, inertia: Float) = this(m, inertia, inertia, inertia)
}
