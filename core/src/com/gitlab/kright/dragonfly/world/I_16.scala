/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

/**
  * An I-16 model
  *
  * Created by Igor Slobodskov on 30.08.2015.
  */
class I_16(engine: iEngineSystem,
           physicsBody: PhysicsBody,
           aerodynamicModel: iAerodynamicModel) extends AirplaneModelCompound(engine, physicsBody, aerodynamicModel) {

  def this() = this(new SimpleEngine(750 * 0.7355f * 1000, 1.5f, 20), new PhysicsBody(1400, 3080, 5520, 2440), new SimpleAerodynamics)

}
