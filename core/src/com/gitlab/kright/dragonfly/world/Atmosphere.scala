/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.Vector3

/**
  * model of atmosphere
  *
  * may be I'll add wind
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class Atmosphere(val base_density: Float = 1.22f, val base_temperature: Float = 300) {

  private val L = 0.0065f

  private val pow = (WorldParams.g * 0.0289644f) / (8.31447 * L) - 1

  def temperature(position: Vector3): Float = base_temperature - L * position.y

  def airDensity(position: Vector3): Float = base_density * Math.pow(temperature(position) / base_temperature, pow).toFloat
}
