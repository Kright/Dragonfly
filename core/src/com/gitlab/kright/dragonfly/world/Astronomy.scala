/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{MathUtils, Quaternion, Vector3}
import com.gitlab.kright.dragonfly
import com.gitlab.kright.dragonfly.engine.math._

class Astronomy(north: Vector3, zenith: Vector3, val latitude: Float) {

  private val seasonBonus = 12
  // todo add as result of time in years, -23.5 at winter , 23.5 at summer
  private val sunRot = new Quaternion()
  private val polarStarDirection: Vector3 = {
    val h = latitude * MathUtils.degreesToRadians
    (north * Math.cos(h).toFloat) + (zenith * Math.sin(h).toFloat)
  }

  private val sunBasePos = {
    val angle = MathUtils.degreesToRadians * (latitude + 90 - seasonBonus)
    (dragonfly.world.WorldParams.north * Math.cos(angle).toFloat) + (dragonfly.world.WorldParams.zenith * Math.sin(angle).toFloat)
  }

  /**
    * @param daytime : 0 - midnight of first day, 1 - midnight os second day
    * @param result
    * @return
    */
  def getSunPosition(daytime: Double, result: Vector3 = new Vector3()): Vector3 = {
    sunRot := (polarStarDirection, (-daytime * (2.0 * Math.PI)).toFloat)
    result := sunBasePos
    sunRot *>> result
  }

}
