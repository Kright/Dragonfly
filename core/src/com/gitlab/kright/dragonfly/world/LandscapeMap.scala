/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Rectangle, Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.interfaces.iLandscapeMap

/**
  * land height map
  *
  * Created by Igor Slobodskov on 27.07.2015.
  */
class LandscapeMap(val bounds: Rectangle,
                   val amplitude: Float,
                   val persistence: Float = 0.4f,
                   val iterations: Int = 6)
  extends iLandscapeMap {

  private def noise(x: Int): Float = 1.0f - ((x * (x * x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0f

  override def getHeightAndNormal(position: Vector2, result: Vector3, dx: Float): Float = {
    val p = position
    val h = getHeight(position)
    result.set(getHeight(p.x - dx, p.y) - getHeight(p.x + dx, p.y), 2 * dx, getHeight(p.x, p.y - dx) - getHeight(p.x, p.y + dx))
    result.normalize()
    h
  }

  private def mix(left: Float, right: Float, t: Float): Float = left + t * (right - left)

  private def mix(lu: Float, ld: Float, ru: Float, rd: Float, tx: Float, ty: Float): Float = {
    val up = mix(lu, ru, tx)
    val down = mix(ld, rd, tx)
    mix(up, down, ty)
  }

  private def getHeight(x: Float, y: Float): Float = {
    var dx = (x - bounds.x) / bounds.width
    var dy = (y - bounds.y) / bounds.height

    if (dx < 0) dx = 0
    if (dy < 0) dy = 0

    var size = 1
    var shift = 0
    var result = 0f
    var amp = amplitude

    for (_ <- 1 to iterations) {
      val xn = (dx * size).toInt
      val yn = (dy * size).toInt

      val tx = dx * size - xn
      val ty = dy * size - yn

      assert(tx >= 0f && ty >= 0 && tx <= 1f && ty <= 1f)

      def h(x: Int, y: Int) = noise(1001 * (shift + x + y * (size + 1)))

      result += amp * mix(h(xn, yn), h(xn, yn + 1), h(xn + 1, yn), h(xn + 1, yn + 1), tx, ty)

      shift += (size + 1) * (size + 1)
      size *= 2
      amp *= persistence
    }

    result
  }

  override def getHeight(position: Vector2): Float = getHeight(position.x, position.y)
}
