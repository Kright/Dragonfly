/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Quaternion}
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  * abstract airplane model
  *
  * Created by Igor Slobodskov on 18.08.2015.
  */
trait Airplane extends iPhysicsBody {

  def mass: Float

  def I: Matrix4

  private val tempM = new Matrix4()
  private val tempQ = new Quaternion()

  def getInertiaTensor(orientation: Quaternion): Matrix4 = {
    tempM.set(orientation)
    tempM.mul(I)
    tempQ := orientation
    tempQ.conjugate()
    tempM.rotate(tempQ)
    tempM
  }

  def getForceAndMoment(state: State, input: PlaneInput): Derivative
}
