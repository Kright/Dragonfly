/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Vector3}
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.engine.opengl.GLHelper
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  * extremely simple realization
  *
  * Created by Igor Slobodskov on 18.08.2015.
  */
class AirplaneNaive extends Airplane {

  override def mass: Float = 900

  val I: Matrix4 = GLHelper.makeMatrix4FromTrace(mass, mass, mass)

  override def getForceAndMoment(state: State, input: PlaneInput): Derivative = {
    new Derivative(new Vector3, new Vector3(input.pitch, -input.yaw, -input.roll) * 2000)
  }
}
