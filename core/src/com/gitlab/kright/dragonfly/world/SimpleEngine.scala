/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Vector3}
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  * Created by Igor Slobodskov on 30.08.2015.
  */
class SimpleEngine(val maxPower: Float, D: Float, val minSpeed: Float) extends iEngineSystem {

  private val S = Math.PI.toFloat / 4f * D * D

  override def modifyI(I: Matrix4): Unit = {
    //nothing
  }

  private val forward = new Vector3
  private val wind = new Vector3

  override def getForceAndWind(state: State, input: PlaneInput, result: Derivative): Vector3 = {
    state.position.rot.getZ(forward)
    forward *= -1


    val speed = Math.max(minSpeed, state.speed.linear dot forward)
    val force = input.engineTrust * maxPower / speed
    result.linear.madd(forward, force)

    // val airDensity = WorldParams.atmosphere.airDensity(state.position.pos)

    wind := forward
    wind *= -force / S / speed
    wind
  }
}
