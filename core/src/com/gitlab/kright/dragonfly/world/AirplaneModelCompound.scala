/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Vector3}
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.playerio.PlaneInput


/**
  * abstract model, compound of components like engine, aerodynamic model and etc
  *
  * Created by Igor Slobodskov on 30.08.2015.
  */
abstract class AirplaneModelCompound(protected val engine: iEngineSystem,
                                     protected val physicsBody: iPhysicsBody,
                                     protected val aerodynamicModel: iAerodynamicModel) extends Airplane {

  override def mass: Float = physicsBody.mass

  private val inertia = new Matrix4()

  override def I: Matrix4 = {
    inertia.set(physicsBody.I)
    engine.modifyI(inertia)
    inertia
  }

  override def getForceAndMoment(state: State, input: PlaneInput): Derivative = {
    val forces = new Derivative()
    forces.linear.madd(WorldParams.gVect, mass)

    val wind = engine.getForceAndWind(state, input, forces)
    aerodynamicModel.addForceAndMoment(state, input, wind, forces)
    forces
  }
}

trait iEngineSystem {

  def modifyI(I: Matrix4): Unit

  def getForceAndWind(state: State, input: PlaneInput, result: Derivative): Vector3
}

trait iAerodynamicModel {

  def addForceAndMoment(state: State, input: PlaneInput, wind: Vector3, result: Derivative): Unit

}