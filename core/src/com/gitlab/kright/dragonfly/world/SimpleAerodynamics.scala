/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Vector3}
import com.gitlab.kright.dragonfly.engine.math.{Derivative, _}
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  * Created by Igor Slobodskov on 30.08.2015.
  */
class SimpleAerodynamics extends iAerodynamicModel {

  val planesS = 14.5f
  val planeSVert = 2f
  val crossSectionalArea = 0.5f
  val maxAngle = 0.3f

  private def cy(angle: Float) = {
    val pi = 3.1415928535f
    if (angle > maxAngle) (pi - angle) / (pi - maxAngle)
    else if (angle < -maxAngle) -(pi + angle) / (pi - maxAngle)
    else angle / maxAngle
  }

  private def liftingForce(speed2: Float, angle: Float, airDensity: Float): Float =
    cy(angle) * airDensity * speed2 * 0.5f * planesS

  private def steerForce(speed2: Float, angle: Float, airDensity: Float): Float =
    angle * airDensity * speed2 * 0.5f * planeSVert

  private def dragForce(speed2: Float, attackAngle: Float, steeringAngle: Float, airDensity: Float): Float = {
    speed2 * (0.5f * airDensity * crossSectionalArea
      + Math.abs(attackAngle) * 0.1f * planesS
      + Math.abs(steeringAngle) * 0.1f * planeSVert)
  }

  private val toWorld = new Matrix4
  private val fromWorld = new Matrix4

  private val forceLocal = new Vector3
  private val spLocal = new Vector3
  private val spAngLocal = new Vector3

  override def addForceAndMoment(state: State, input: PlaneInput, wind: Vector3, result: Derivative): Unit = {
    toWorld.set(state.position.rot)
    fromWorld.set(toWorld).tra

    forceLocal := (0, 0, 0)
    spLocal := state.speed.linear
    spLocal.mul(fromWorld)
    spAngLocal := state.speed.angular
    spAngLocal.mul(fromWorld)

    // val speed = state.speed.linear
    val sp2 = state.speed.linear.length2

    val airDensity = WorldParams.atmosphere.airDensity(state.position.pos)

    val attackAngle = -Math.asin(spLocal.y / (0.1f + spLocal.length)).toFloat
    val steeringAngle = Math.asin(spLocal.x / (0.1 + spLocal.length)).toFloat

    forceLocal.y += liftingForce(sp2, attackAngle, airDensity)
    forceLocal.x -= steerForce(sp2, steeringAngle, airDensity)
    forceLocal += spLocal * (-dragForce(spLocal.length, attackAngle, steeringAngle, airDensity))

    val mul = spLocal.z * spLocal.z * airDensity

    result.angular.x = (input.pitch + 0.3f) * mul * 2f
    result.angular.y = -input.yaw * mul * 1f
    result.angular.z = -input.roll * mul * 5

    result.angular.x -= attackAngle * airDensity * mul * 5f
    result.angular.y -= steeringAngle * airDensity * mul * 5f

    result.angular.madd(spAngLocal, -airDensity * 5000f)
    result.angular.z += forceLocal.y * steeringAngle * 1f

    result.angular.mul(state.position.rot)

    forceLocal.mul(toWorld)
    result.linear += forceLocal
  }
}
