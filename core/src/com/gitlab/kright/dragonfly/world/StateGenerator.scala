/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.world

import com.badlogic.gdx.math.{Matrix4, Vector3}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.playerio.PlaneInput

/**
  * It generates next states for plane
  *
  * Created by Igor Slobodskov on 18.08.2015.
  */
class StateGenerator {

  //for calculations
  private val inversed = new Matrix4()
  private val omega = new Vector3()
  private val iOm = new Vector3()
  private val angularAcceleration = new Vector3()

  def nextState(now: State, timeStep: Long, model: Airplane, planeInput: PlaneInput): State = {
    assert(timeStep > 0)

    val dt = timeStep * 0.001f
    val next = new State(now.time + timeStep)

    next.position := now.position
    next.speed := now.speed

    val forces = model.getForceAndMoment(now, planeInput)

    //linear
    next.speed.linear += forces.linear * (dt / model.mass)
    next.position.pos += (now.speed.linear + next.speed.linear) * (0.5f * dt)

    //angular
    val I = model.getInertiaTensor(now.position.rot)
    inversed.set(I).inv()

    omega := now.speed.angular
    iOm := omega
    iOm.mul(I)

    angularAcceleration.setCross(iOm, omega)

    angularAcceleration += forces.angular
    angularAcceleration.mul(inversed)

    next.speed.angular.madd(angularAcceleration, dt)
    next.speed.angular *= 1f - dt * 0.1f //friction

    val angSp = (next.speed.angular + now.speed.angular) / 2

    next.position.rot := angSp * dt
    next.position.rot *= now.position.rot

    next
  }
}
