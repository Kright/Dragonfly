/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.utils

import com.badlogic.gdx.utils.Disposable

class LazyDisposable[T <: Disposable](constructor: => T) extends Disposable {

  private var obj: Disposable = null

  def apply(): T = {
    if (obj == null) {
      obj = constructor
    }
    obj.asInstanceOf[T]
  }

  override def dispose(): Unit =
    if (obj != null) {
      obj.dispose()
      obj = null
    }
}

object LazyDisposable {
  def apply[T <: Disposable](constructor: => T): LazyDisposable[T] = new LazyDisposable(constructor)
}
