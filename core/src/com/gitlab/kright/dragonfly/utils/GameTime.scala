/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.utils

class GameTime(var totalMs: Long = 0) {

  def shift(delta: Long): Unit = totalMs += delta

  def seconds: Double = totalMs / 1000.0
}
