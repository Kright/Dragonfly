/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.{Mesh, VertexAttribute}
import com.badlogic.gdx.math.{Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.{FloatBuf, ShortBuf}

import scala.util.Random

/**
  * clouds as rectangles
  *
  * Created by Igor Slobodskov on 30.07.2015.
  */
object CloudsMesh {

  def generateClouds(cloudsCount: Int, areaS: Float): Mesh = {
    val rnd = new Random()

    val floats = new FloatBuf(cloudsCount * 7 * 4)

    val position = new Vector3()
    val lu = new Vector2(0, 0)
    val ld = new Vector2(0, 1)
    val ru = new Vector2(1, 0)
    val rd = new Vector2(1, 1)

    val center = new Vector2(0.5f, 0.5f)
    val cloudSize = new Vector2(500, 200)
    val scaleLU = (lu - center).scl(cloudSize)
    val scaleLD = (ld - center).scl(cloudSize)
    val scaleRU = (ru - center).scl(cloudSize)
    val scaleRD = (rd - center).scl(cloudSize)

    def put(v: Vector3, v2: Vector2, v3: Vector2): Unit = {
      floats += v
      floats += v2
      floats += v3
    }

    for (_ <- 0 until cloudsCount) {
      position := (-areaS + (2f * areaS) * rnd.nextFloat(), 900 + 200f * rnd.nextFloat(), -areaS + (2f * areaS) * rnd.nextFloat())
      if (rnd.nextBoolean()) position.y += 3000
      val ds = 0.5f + rnd.nextFloat()
      put(position, scaleLU * ds, lu)
      put(position, scaleRU * ds, ru)
      put(position, scaleLD * ds, ld)
      put(position, scaleRD * ds, rd)
    }

    val indices = new ShortBuf(6 * cloudsCount)
    for (i <- 0 until cloudsCount) {
      val pos = 4 * i
      indices += (pos, pos + 2, pos + 1)
      indices += (pos + 1, pos + 2, pos + 3)
    }

    val mesh = new Mesh(true, floats.size, cloudsCount * 6,
      new VertexAttribute(Usage.Position, 3, "aPos"),
      new VertexAttribute(Usage.Position, 2, "aScale"),
      new VertexAttribute(Usage.TextureCoordinates, 2, "aTexPos"))

    mesh.setVertices(floats.getArray())
    mesh.setIndices(indices.getArray())

    mesh
  }
}
