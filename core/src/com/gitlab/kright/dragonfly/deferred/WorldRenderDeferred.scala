/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL20._
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.WorldRender
import com.gitlab.kright.dragonfly.engine.{Camera, Disposer}
import com.gitlab.kright.dragonfly.engine.math.ConstPoint2i
import com.gitlab.kright.dragonfly.interfaces.VisibleState

class WorldRenderDeferred(val screenSize: ConstPoint2i) extends WorldRender with Disposable {

  private val scope = new Disposer()

  private val quad = scope(new VaoQuad2d())

  private val airplaneRender = scope(new AirplaneRenderPass())
  private val landscapeRender = scope(new LandscapeRenderPass())
  private val cloudsRender = scope(new CloudsRenderPass())
  private val decalsRender = scope(new DecalsRender())
  private val skyRender = scope(new SkyRenderPass(quad.vao))
  private val sunLightPass = scope(new SunLightPass(quad.vao))
  private val sunDiskRender = scope(new SunDiskRender(quad.vao))
  private val atmospherePass = scope(new AtmospherePass(quad.vao))
  private val toneMappingPass = scope(new ToneMappingPass(quad.vao))

  private val gBuffer = scope(new GBuffer(screenSize))
  private val gBufferColorHDR = scope(new GBufferColorHDR(screenSize))
  private val gBufferAtmosphereHDR = scope(new GBufferColorHDR(screenSize))

  override def render(camera: Camera, airplanes: Seq[VisibleState], skyRenderParams: SkyRenderParams, decalMatrix: Matrix4): Unit = {
    val gl30 = Gdx.gl30
    import gl30._

    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)
    glFrontFace(GL_CW)

    glViewport(0, 0, screenSize.x, screenSize.y)

    gBuffer.frameBuffer.binded {
      glClear(GL_DEPTH_BUFFER_BIT)

      airplaneRender.render(camera, airplanes)
      cloudsRender.render(camera)
      landscapeRender.render(camera)
      // todo in future add parallax mapping
      skyRender.render()
    }

    glFlush()

    glDisable(GL_DEPTH_TEST)
    gBuffer.decalsFrameBuffer.binded{
      glDepthMask(false)
      decalsRender.render(camera, decalMatrix, gBuffer.linearDepth)
      glDepthMask(true)
    }

    glFlush()

    glViewport(0, 0, gBufferColorHDR.size.x, gBufferColorHDR.size.y)

    gBufferColorHDR.frameBuffer.binded {
      // todo change passing gBuffer to passing textures
      sunLightPass.render(camera, skyRenderParams, gBuffer.albedoRoughness, gBuffer.normalSpec)
      sunDiskRender.render(camera, skyRenderParams, gBuffer.linearDepth)
      // todo add sun halo rendering
      // may be clouds would be better to render here
    }

    glFlush()

    gBufferAtmosphereHDR.frameBuffer.binded {
      atmospherePass.render(camera, skyRenderParams, gBufferColorHDR.colorHDR, gBuffer.linearDepth)
    }

    glFlush()

    glViewport(0, 0, screenSize.x, screenSize.y)

    toneMappingPass.render(camera, gBufferAtmosphereHDR.colorHDR)

    Gdx.gl30.glActiveTexture(GL20.GL_TEXTURE0) // to avoid bug in font rendering
    // todo implement PBR!
    // todo add bloom
  }

  override def dispose(): Unit = scope.dispose()
}
