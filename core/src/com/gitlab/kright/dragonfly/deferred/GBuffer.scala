/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.{GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT}
import com.badlogic.gdx.graphics.GL30.{GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2}
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Disposer
import com.gitlab.kright.dragonfly.engine.math.ConstPoint2i
import com.gitlab.kright.dragonfly.engine.opengl.{FramebufferGL, GLHelper, InternalFormat, TextureFormat, TextureGL, TextureType, Utils}

class GBuffer(val size: ConstPoint2i) extends Disposable {

  private val scope = new Disposer()

  val albedoRoughness: TextureGL = scope(new TextureGL() {
    binded {
      glTexImage2d(0, InternalFormat.GL_RGBA8, size.x, size.y, 0, TextureFormat.GL_RGBA, TextureType.GL_UNSIGNED_BYTE)
      setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
      GLHelper.glCheckError()
    }
  })

  val normalSpec: TextureGL = scope(new TextureGL() {
    binded {
      glTexImage2d(0, InternalFormat.GL_RGBA, size.x, size.y, 0, TextureFormat.GL_RGBA, TextureType.GL_UNSIGNED_BYTE)
      setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
      GLHelper.glCheckError()
    }
  })

  val linearDepth: TextureGL = scope(new TextureGL() {
    binded {
      glTexImage2d(0, InternalFormat.GL_R16F, size.x, size.y, 0, TextureFormat.GL_RED, TextureType.GL_HALF_FLOAT)
      setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
      GLHelper.glCheckError()
    }
  })

  val depth: TextureGL = scope(new TextureGL() {
    binded {
      GLHelper.glCheckError()
      glTexImage2d(0, InternalFormat.GL_DEPTH_COMPONENT24, size.x, size.y, 0, TextureFormat.GL_DEPTH_COMPONENT, TextureType.GL_UNSIGNED_INT)
      GLHelper.glCheckError()
      setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
      GLHelper.glCheckError()
    }
  })

  val frameBuffer: FramebufferGL = scope(new FramebufferGL() {
    binded {
      glTexture2d(GL_COLOR_ATTACHMENT0, albedoRoughness.target, albedoRoughness.id)
      glTexture2d(GL_COLOR_ATTACHMENT1, normalSpec.target, normalSpec.id)
      glTexture2d(GL_COLOR_ATTACHMENT2, linearDepth.target, linearDepth.id)

      Gdx.gl30.glDrawBuffers(3, Utils.makeIntBuffer(Array(GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2)))
      assertIsComplete()

      glTexture2d(GL_DEPTH_ATTACHMENT, depth.target, depth.id)
      assertIsComplete()
    }
  })

  val decalsFrameBuffer: FramebufferGL = scope(new FramebufferGL() {
    binded {
      glTexture2d(GL_COLOR_ATTACHMENT0, albedoRoughness.target, albedoRoughness.id)

      Gdx.gl30.glDrawBuffers(1, Utils.makeIntBuffer(Array(GL_COLOR_ATTACHMENT0)))
      assertIsComplete()

      glTexture2d(GL_DEPTH_ATTACHMENT, depth.target, depth.id)
      assertIsComplete()
    }
  })

  override def dispose(): Unit = scope.dispose()
}
