/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.{GL20, Mesh, VertexAttribute}
import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.{Camera, Disposer}
import com.gitlab.kright.dragonfly.engine.opengl.Shader


class GizmoRender extends Disposable {
  private val scope = new Disposer()
  private val shader = scope(Shader.loadFromAssets("deferred/gizmo"))

  private val meshLines = scope(new MeshWrapper(GL20.GL_LINES))
  private val meshPoints = scope(new MeshWrapper(GL20.GL_POINTS))

  def render(camera: Camera, gizmoBuilder: GizmoBuilder): Unit = {
    meshLines.setData(gizmoBuilder.getLinesArray)
    meshPoints.setData(gizmoBuilder.getPointsArray)

    shader.run {
      shader.uViewProjection = camera.viewProjection

      meshLines.render(shader)
      meshPoints.render(shader)
    }
  }

  override def dispose(): Unit = scope.dispose()
}

private final class MeshWrapper(val glRenderMode: Int) extends Disposable {
  private val vertexAttributes = Seq(
    new VertexAttribute(Usage.Position, 3, "aPos"),
    new VertexAttribute(Usage.Position, 3, "aColor")
  )
  private var mesh: Option[Mesh] = None
  private var verticesCount: Int = 0

  def setData(data: Array[Float]): Unit = {
    assert(data.size % 6 == 0)
    verticesCount = data.size / 6

    mesh = Option(
      mesh.filter(_.getMaxVertices >= data.size)
        .getOrElse(new Mesh(false, verticesCount, 0, vertexAttributes: _*))
        .setVertices(data)
    )
  }

  def render(shader: Shader): Unit =
    mesh.foreach {
      _.render(shader.program, glRenderMode, 0, verticesCount, true)
    }

  override def dispose(): Unit = {
    mesh.foreach(_.dispose())
    mesh = None
  }
}