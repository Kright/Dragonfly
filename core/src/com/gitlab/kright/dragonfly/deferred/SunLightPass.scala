/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_TRIANGLE_STRIP
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureGL, VertexArrayObjectGL}

class SunLightPass(quad2d: VertexArrayObjectGL) extends Disposable {

  private val shader = Shader.loadFromAssets("deferred/sun_light")

  def render(camera: Camera, renderParams: SkyRenderParams, albedoRoughness: TextureGL, normalSpec: TextureGL): Unit = {
    shader.run {
      quad2d.binded {
        shader.gAlbedoRoughness = albedoRoughness.activeTexture(0)
        shader.gNormalSpec = normalSpec.activeTexture(1)
        shader.uSunPosition = renderParams.sunPosition
        shader.uSunLight = renderParams.sunLight
        shader.uSkyLight = renderParams.skyLight
        shader.gamma = camera.gamma

        Gdx.gl30.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
      }
    }
  }

  override def dispose(): Unit = {
    shader.dispose()
  }
}
