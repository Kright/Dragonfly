/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.{Mesh, VertexAttribute}
import com.badlogic.gdx.math.{Rectangle, Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.{FloatBuf, ShortBuf}
import com.gitlab.kright.dragonfly.world
import com.gitlab.kright.dragonfly.engine.math._

/**
  * it renders mesh
  *
  * Created by Igor Slobodskov on 27.07.2015.
  */
object LandscapeMesh {

  // todo rm generated mesh, add loading from obj

  def generateMesh(): Mesh = {
    val size = 128
    val delta = 100f
    val mapSize = size * delta

    val hMap = new world.LandscapeMap(new Rectangle(-mapSize / 2, -mapSize / 2, mapSize, mapSize), 3 * delta, 0.5f, 8)

    val verticesCount = (size + 1) * (size + 1) * (3 + 3)
    val indicesCount = size * size * 6

    val vertices = {
      val mapPos = new Vector2()
      val normal = new Vector3()
      val arr = new FloatBuf(verticesCount)

      for (ny <- 0 to size)
        for (nx <- 0 to size) {
          mapPos := (nx - 0.5f * size, ny - 0.5f * size)
          mapPos *= delta

          val distanceToBorder = math.min(math.min(nx, ny), math.min(size - nx, size-ny)).toFloat
          val borderBonus = 0.5f * (1.0 - math.cos(math.min(20f, distanceToBorder) / 20 * math.Pi)).toFloat
          val h = borderBonus * hMap.getHeightAndNormal(mapPos, normal, delta)

          arr += (mapPos.x, h, mapPos.y)
          arr += normal
        }
      arr
    }

    val indices = {
      val arr = new ShortBuf(indicesCount)

      for (j <- 0 until size) {
        for (i <- 0 until size) {
          val p1 = i + j * (size + 1)
          val p2 = p1 + 1
          val p3 = i + (j + 1) * (size + 1)
          val p4 = p3 + 1
          arr += (p1, p2, p3)
          arr += (p3, p2, p4)
        }
      }
      arr
    }

    val mesh = new Mesh(true, verticesCount, indicesCount,
      new VertexAttribute(Usage.Position, 3, "aPos"),
      new VertexAttribute(Usage.Normal, 3, "aNormal"))

    mesh.setVertices(vertices.getArray())
    mesh.setIndices(indices.getArray())

    mesh
  }
}
