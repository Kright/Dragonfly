/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.{GL20, Texture}
import com.badlogic.gdx.math.{Matrix3, Matrix4, Vector2}
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureLoader}
import com.gitlab.kright.dragonfly.engine.math._


class LandscapeRenderPass extends Disposable {

  private val texture = TextureLoader(name = "textures/land4096.png", filterMag = Texture.TextureFilter.Linear)
  private val shader = Shader.loadFromAssets("deferred/landscape")
  private val idMat3 = new Matrix3()
  private val mesh = LandscapeMesh.generateMesh()

  def render(camera: Camera): Unit = {
    val modelMatrix = new Matrix4()
    val tmp = new Matrix4()
    val step = 12800f
    val scale = 1f
    for (dx <- -1 to 1) {
      for (dy <- -1 to 1) {
        modelMatrix.idt().scale(scale, scale, scale).translate(step * dx, 0, step * dy)
        shader.run {
          tmp := modelMatrix
          camera.viewProjection *>> tmp
          shader.uMatrix = tmp

          tmp := modelMatrix
          camera.view *>> tmp
          shader.uModelView = tmp

          shader.uNormalMatrix = idMat3

          texture.bind(0)
          shader.uTexture = 0

          shader.uTexturePos = new Vector2(-0.5f, -0.5f)
          shader.uTextureScale = 0.01f / 128f

          mesh.render(shader.program, GL20.GL_TRIANGLES)
        }
      }
    }
  }

  override def dispose(): Unit = {
    mesh.dispose()
    texture.dispose()
    shader.program.dispose()
  }
}
