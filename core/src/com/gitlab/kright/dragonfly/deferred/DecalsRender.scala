/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.{GL20, Mesh, Texture, VertexAttribute}
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureGL, TextureLoader, Utils}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.{Camera, Disposer}

class DecalsRender extends Disposable {
  private val scope = new Disposer()

  private val shader = scope(Shader.loadFromAssets("deferred/decals"))
  private val colorTexture = scope(TextureLoader("textures/cloud2.png", useSRGB = true, wrap = Texture.TextureWrap.ClampToEdge, filterMin = Texture.TextureFilter.MipMapLinearLinear, filterMag = Texture.TextureFilter.Linear))

  private val cubeMesh: Mesh = scope{
    val cubeStrip = Utils.makeCubeTrianglesStrip()
    val mesh = new Mesh(true, cubeStrip.size / 3, 0,
      new VertexAttribute(Usage.Position, 3, "aPos")
    )
    mesh.setVertices(cubeStrip)
    mesh
  }

  private val tmp = new Matrix4()
  private val bias = new Matrix4().translate(0.5f, 0.5f, 0.5f).scale(0.5f, 0.5f, 0.5f)

  def render(camera: Camera, model: Matrix4, linearDepth: TextureGL): Unit =
    shader.run{
      tmp := camera.viewProjection
      tmp *= model
      shader.uMVP = tmp

      tmp := camera.view
      tmp *= model
      tmp.inv()
      bias *>> tmp

      shader.uModelViewInvBias = tmp

      shader.uCamRxRy = camera.rSize

      shader.uLinearDepthTexture = linearDepth.activeTexture(0)
      colorTexture.bind(1)
      shader.uColorTexture = 1

      cubeMesh.render(shader.program, GL20.GL_TRIANGLE_STRIP)
    }

  override def dispose(): Unit = scope.dispose()
}
