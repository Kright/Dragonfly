/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.{GL20, Texture}
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureLoader}

class CloudsRenderPass extends Disposable {

  private val texture = TextureLoader("textures/cloud2.png", filterMag = Texture.TextureFilter.Linear, useSRGB = true)
  private val shader = Shader.loadFromAssets("deferred/clouds")
  private val mesh = CloudsMesh.generateClouds(cloudsCount = 400, areaS = 6400f)

  private val tmpUp = new Vector3()
  private val tmpRight = new Vector3()

  def render(camera: Camera): Unit = {
    shader.run {
      texture.bind(0)
      shader.uTexture = 0

      shader.uMatrix = camera.viewProjection
      shader.uModelView = camera.view

      tmpRight := (-camera.forward.z, 0, camera.forward.x)
      shader.uRight = tmpRight.normalize()

      tmpUp.setCross(tmpRight, camera.forward)
      shader.uUp = tmpUp

      mesh.render(shader.program, GL20.GL_TRIANGLES)
    }
  }

  override def dispose(): Unit = {
    texture.dispose()
    shader.dispose()
    mesh.dispose()
  }
}
