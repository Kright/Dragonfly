/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20._
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, VertexArrayObjectGL}

class SkyRenderPass(quad2d: VertexArrayObjectGL) extends Disposable {
  private val shader = Shader.loadFromAssets("deferred/sky")

  def render(): Unit = {
    val gl30 = Gdx.gl30
    import gl30._

    shader.run {
      quad2d.binded {
        shader.uSkyLinearDepth = 60000.0f
        shader.uSkyDepth = 1.0f
        glDepthFunc(GL_LEQUAL)
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
        glDepthFunc(GL_LESS)
      }
    }
  }

  override def dispose(): Unit = {
    shader.dispose()
  }
}
