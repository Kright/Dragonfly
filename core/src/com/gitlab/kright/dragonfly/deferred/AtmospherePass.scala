/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_TRIANGLE_STRIP
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureGL, VertexArrayObjectGL}

class AtmospherePass(quad2d: VertexArrayObjectGL) extends Disposable {

  private val shader = Shader.loadFromAssets("deferred/atmosphere")

  def render(camera: Camera, renderParams: SkyRenderParams, colorHDR: TextureGL, linearDepth: TextureGL): Unit = {
    shader.run {
      quad2d.binded {
        shader.gColorHDR = colorHDR.activeTexture(0)
        shader.gLinearDepth = linearDepth.activeTexture(1)

        shader.uCamRxRy = camera.rSize
        shader.uCamForward = camera.orientation.getZ() * -1f
        shader.uCamRight = camera.orientation.getX()
        shader.uCamTop = camera.orientation.getY()
        shader.uCamPos = camera.pos

        shader.uSkyLight = renderParams.skyLight
        shader.uAirColorDensityKm = renderParams.airColorDensityKm
        shader.uAirColorDensityHalfHeightKm = renderParams.airColorDensityHalfHeightKm

        Gdx.gl30.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
      }
    }
  }

  override def dispose(): Unit = {
    shader.dispose()
  }
}
