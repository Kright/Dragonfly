/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.math.Vector3
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.utils.GameTime
import com.gitlab.kright.dragonfly.world.WorldParams

class SkyRenderParams {

  val skyColor = new Vector3()
  val sunColor = new Vector3()

  val sunLight = new Vector3()
  val skyLight = new Vector3()

  val sunPosition = new Vector3()

  val airColorDensityHalfHeightKm: Float = 6
  val airColorDensityKm: Vector3 = {
    // https://en.wikipedia.org/wiki/Visibility
    val d = 0.1f // 0.03f
    new Vector3(d, d * 2, d * 4)
  }

  def update(airplanePosition: Vector3, gameTime: GameTime): Unit = {
    //todo rework light model
    val baseSunLight = new Vector3(0.9f, 1f, 1f)
    val nightLight = new Vector3(1f, 1f, 2f) / 1000000f

    val msToDays: Double = 0.001 / 60

    WorldParams.astronomy.getSunPosition(msToDays * gameTime.totalMs, sunPosition)

    val sin = sunPosition dot WorldParams.zenith
    val dens =
      if (sin > 0f) {
        1f / (sin + 0.1f)
      } else {
        10f - 100f * sin
      }

    // 0.2 because ~75% transparency in best case, and usual about 1/3
    val real_dens = 0.2f * dens * WorldParams.atmosphere.airDensity(airplanePosition)

    def exp(f: Float) = Math.exp(f).toFloat

    val sc = new Vector3(exp(-real_dens), exp(-real_dens * 2), exp(-real_dens * 4))

    val b = baseSunLight
    sunLight := (sc.x * b.x, sc.y * b.y, sc.z * b.z)

    skyLight := (b.x * (1 - sc.x), b.y * (1 - sc.y), b.z * (1 - sc.z))

    val eps = 0.03f

    if (sin > 0f) {
      skyLight *= sin + eps
    } else {
      skyLight *= eps * eps / (eps - sin)
    }

    skyLight += nightLight

    val luminance = (sunLight + skyLight).length / Math.sqrt(3).toFloat

    val correction = (1f + Math.log(luminance).toFloat / 7) / luminance

    sunLight *= correction
    skyLight *= correction
  }
}
