/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.math.{Matrix4, Quaternion, Vector3}
import com.gitlab.kright.dragonfly.engine.math._

import scala.collection.mutable.ArrayBuffer


class GizmoBuilder {

  private val dataLines = new ArrayBuffer[Float]()
  private val dataPoints = new ArrayBuffer[Float]()
  private val tmpVec = new Vector3()

  private def push(arr: ArrayBuffer[Float], v: Vector3): Unit = {
    arr += v.x
    arr += v.y
    arr += v.z
  }

  private def pushPosColor(arr: ArrayBuffer[Float], pos: Vector3, color: Vector3): Unit = {
    push(arr, pos)
    push(arr, color)
  }

  private def mul(matrix: Matrix4, vec: Vector3): Vector3 = {
    tmpVec := vec
    matrix *>> tmpVec
    tmpVec
  }

  def addLine(from: Vector3, to: Vector3, color: Vector3): Unit = {
    pushPosColor(dataLines, from, color)
    pushPosColor(dataLines, to, color)
  }

  def addLine(from: Vector3, to: Vector3, color: Vector3, modelMatrix: Matrix4): Unit = {
    pushPosColor(dataLines, mul(modelMatrix, from), color)
    pushPosColor(dataLines, mul(modelMatrix, to), color)
  }

  def addPoint(position: Vector3, color: Vector3): Unit =
    pushPosColor(dataPoints, position, color)

  def addPoint(position: Vector3, color: Vector3, modelMatrix: Matrix4): Unit =
    pushPosColor(dataPoints, mul(modelMatrix, position), color)

  def addCube(color: Vector3, modelMatrix: Matrix4): Unit = {
    for (i <- Seq(-1f, 1f)) {
      for (j <- Seq(-1f, 1f)) {
        addLine(new Vector3(i, j, -1f), new Vector3(i, j, 1f), color, modelMatrix)
        addLine(new Vector3(i, -1f, j), new Vector3(i, 1f, j), color, modelMatrix)
        addLine(new Vector3(-1f, i, j), new Vector3(1f, i, j), color, modelMatrix)
      }
    }
  }

  def addCube(center: Vector3, size: Vector3 = new Vector3(), orientation: Quaternion = new Quaternion(), color: Vector3 = new Vector3()): Unit =
    addCube(color, new Matrix4().translate(center).rotate(orientation).scale(size.x / 2, size.y / 2, size.z / 2))

  def clear(): Unit = {
    dataLines.clear()
    dataPoints.clear()
  }

  def getLinesArray: Array[Float] = dataLines.toArray

  def getPointsArray: Array[Float] = dataPoints.toArray
}
