/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.graphics.GL20.{GL_FLOAT, GL_STATIC_DRAW}
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.opengl.{Utils, VertexArrayObjectGL, VertexBufferObjectGL}

class VaoQuad2d extends Disposable {
  val vbo: VertexBufferObjectGL = new VertexBufferObjectGL() {
    binded {
      val vertices = Utils.makeFloatBuffer(Array(-1f, 1f, 1f, 1f, -1f, -1f, 1f, -1f))
      setData(vertices, vertices.capacity() * 4, GL_STATIC_DRAW)
    }
  }

  val vao: VertexArrayObjectGL = new VertexArrayObjectGL() {
    binded {
      vbo.binded {
        vbo.glEnableVertexAttribArray(0)
        vbo.glEnableVertexAttribPointer(0, 2, GL_FLOAT)
      }
    }
  }

  override def dispose(): Unit = {
    vao.dispose()
    vbo.dispose()
  }
}
