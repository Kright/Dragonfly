/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL20, Mesh}
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.opengl.Shader
import com.gitlab.kright.dragonfly.engine.{Camera, ObjFile}
import com.gitlab.kright.dragonfly.interfaces.VisibleState


class AirplaneRenderPass extends Disposable {
  private val airplane: Mesh = {
    val f = new ObjFile(Gdx.files.internal("models/airplane1.obj").readString())
    val correction = new Matrix4() {
      this (2, 2) = -1f
    }
    f.removeNormals().makeSmoothNormals().makeTriangulated().transform(correction).makeMesh()
  }

  private val shader = Shader.loadFromAssets("deferred/airplane")

  private val uModel = new Matrix4()
  private val uModelView = new Matrix4()

  def render(camera: Camera, airplanes: Seq[VisibleState]): Unit = {
    shader.run {
      airplanes.foreach { visibleState =>
        uModel.idt().translate(visibleState.position.pos).rotate(visibleState.position.rot)

        shader.uModel = uModel
        shader.uModelView = camera.view *>> (uModelView := uModel)
        shader.uMVP = camera.viewProjection *>> uModel

        airplane.render(shader.program, GL20.GL_TRIANGLES)
      }
    }
  }

  override def dispose(): Unit = {
    airplane.dispose()
    shader.dispose()
  }
}
