/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_COLOR_ATTACHMENT0
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.math.ConstPoint2i
import com.gitlab.kright.dragonfly.engine.opengl.{FramebufferGL, GLHelper, InternalFormat, TextureFormat, TextureGL, TextureType, Utils}

class GBufferColorHDR(val size: ConstPoint2i) extends Disposable {

  val colorHDR: TextureGL = new TextureGL() {
    binded {
      glTexImage2d(0, InternalFormat.GL_RGB16F, size.x, size.y, 0, TextureFormat.GL_RGB, TextureType.GL_HALF_FLOAT)
      setFilter(TextureFilter.Nearest, TextureFilter.Nearest)
      GLHelper.glCheckError()
    }
  }

  val frameBuffer: FramebufferGL = new FramebufferGL() {
    binded {
      glTexture2d(GL_COLOR_ATTACHMENT0, colorHDR.target, colorHDR.id)
      Gdx.gl30.glDrawBuffers(1, Utils.makeIntBuffer(Array(GL_COLOR_ATTACHMENT0)))
      assertIsComplete()
    }
  }

  override def dispose(): Unit = {
    frameBuffer.dispose()
    colorHDR.dispose()
  }
}
