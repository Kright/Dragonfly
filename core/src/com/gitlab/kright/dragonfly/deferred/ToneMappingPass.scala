/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_TRIANGLE_STRIP
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureGL, VertexArrayObjectGL}

class ToneMappingPass(quad2d: VertexArrayObjectGL) extends Disposable {

  private val shader = Shader.loadFromAssets("deferred/tone_mapping")

  def render(camera: Camera, colorHDR: TextureGL): Unit = {
    shader.run {
      quad2d.binded {
        shader.uColorHDR = colorHDR.activeTexture(0)
        shader.gamma = camera.gamma
        shader.exposure = camera.exposure
        Gdx.gl30.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
      }
    }
  }

  override def dispose(): Unit = {
    shader.dispose()
  }
}
