/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.deferred

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_TRIANGLE_STRIP
import com.badlogic.gdx.math.{Matrix4, Vector2}
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.opengl.{Shader, TextureGL, VertexArrayObjectGL}

class SunDiskRender(quad2d: VertexArrayObjectGL) extends Disposable {

  private val shader = Shader.loadFromAssets("deferred/sun_disk")
  private val temp = new Matrix4()

  def render(camera: Camera, renderParams: SkyRenderParams, linearDepth: TextureGL): Unit = {
    shader.run {
      quad2d.binded {
        shader.gLinearDepth = linearDepth.activeTexture(0)
        shader.uSunDepth = 59900f
        shader.uSunPosition = renderParams.sunPosition
        shader.uSunColor = renderParams.sunLight * 100f
        // todo split sunLight and sunColor
        // todo add bloom

        val size = 1f
        // todo fix this
        shader.udSize = new Vector2(size / camera.fov / camera.aspect, size / camera.fov)

        temp := camera.orientation
        temp.tra()
        shader.uViewPojection = camera.projection *>> temp

        Gdx.gl30.glDrawArrays(GL_TRIANGLE_STRIP, 0, 4)
      }
    }
  }

  override def dispose(): Unit = {
    shader.dispose()
  }
}
