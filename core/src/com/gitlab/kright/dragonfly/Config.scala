/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly

object Config {

  val debugGL: Boolean = true
  val drawGizmo: Boolean = true
}
