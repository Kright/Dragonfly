/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.graphics.VertexAttributes.Usage
import com.badlogic.gdx.graphics.{Mesh, VertexAttribute}
import com.badlogic.gdx.math.{Matrix4, Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.math._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * representation of *.obj file
  * methods like makeTriangulated() create new Object
  *
  * Created by Igor Slobodskov on 19.07.2015.
  */

/**
  * number of position, textureCoord and normal in arrays
  */
class VertexData(val v: Int, val vt: Int = -1, val vn: Int = -1) {

  def ==(d: VertexData): Boolean = v == d.v && vt == d.vt && vn == d.vn

  override def equals(o: Any): Boolean = {
    o match {
      case p: VertexData => this == p
      case _ => false
    }
  }

  override def hashCode(): Int = (v << 20) | (vt << 10) | vn
}

/**
  * face - with 3 or more vertices
  */
class Face(val vertices: Array[VertexData]) {

  def this(v1: VertexData, v2: VertexData, v3: VertexData) = this(Array(v1, v2, v3))
}

/**
  * representation of *.obj file
  * methods like makeTriangulated() create new Object
  *
  */
class ObjFile(val vertices: ArrayBuffer[Vector3],
              val normals: ArrayBuffer[Vector3],
              val uvs: ArrayBuffer[Vector2],
              val faces: ArrayBuffer[Face],
              val notParsed: ArrayBuffer[String]) {

  def this() = this(
    new ArrayBuffer[Vector3](),
    new ArrayBuffer[Vector3](),
    new ArrayBuffer[Vector2](),
    new ArrayBuffer[Face](),
    new ArrayBuffer[String]())

  def this(raw: String, uvFlipY: Boolean = true) = {
    this()
    parse(raw, uvFlipY)
    Logging.objFilesLog("loaded, " + this)
    assert(this.isValid())
  }

  override def toString: String = {
    s"ObjFile: " +
      s"vertices(${vertices.size}), " +
      s"normals(${normals.size}), " +
      s"uvs(${uvs.size}), " +
      s"faces(${faces.size}})," +
      s"not parsed(${notParsed.size})"
  }

  def hasNormals: Boolean = normals.nonEmpty

  def hasUVs: Boolean = uvs.nonEmpty

  def vertexSize: Int = {
    var s = 3
    if (hasNormals) s += 3
    if (hasUVs) s += 2
    s
  }

  def isValid(): Boolean = {
    val lenV = vertices.size
    val lenN = normals.size
    val lenVT = uvs.size
    for (f <- faces) {
      if (f.vertices.length < 3) return false
      for (v <- f.vertices) {
        if (v.v < 0 || v.v >= lenV) return false

        if (lenN != 0) {
          if (v.vn < 0 || v.vn >= lenN) return false
        }
        else if (v.vn != -1) return false

        if (lenVT != 0) {
          if (v.vt < 0 || v.vt >= lenVT) return false
        }
        else if (v.vt != -1) return false
      }
    }
    true
  }

  def isTriangulated(): Boolean = faces.forall(_.vertices.length == 3)

  def makeTriangulated(): ObjFile = {
    if (isTriangulated()) return this //it is already triangulated

    val newFaces = new ArrayBuffer[Face](faces.size)
    for (f <- faces) {
      for (i <- 0 until f.vertices.length - 2) {
        newFaces += new Face(f.vertices(i), f.vertices(i + 1), f.vertices(i + 2))
      }
    }

    new ObjFile(vertices, normals, uvs, newFaces, notParsed)
  }

  def makeMesh(): Mesh = makeTriangulated().makeMeshWithIndices()

  /**
    * works only if every every face has own single normal
    * for example, after blender export with normals
    *
    * @return
    */
  def makeSmoothNormals(): ObjFile = {
    val linksArr = new Array[ArrayBuffer[Face]](vertices.size)
    for (i <- vertices.indices) linksArr(i) = new ArrayBuffer[Face](8)

    for (f <- faces)
      for (v <- f.vertices)
        linksArr(v.v) += f

    val newNormals = new ArrayBuffer[Vector3](vertices.size)
    val v = new Vector3()
    for (links <- linksArr) {
      v := (0, 0, 0)
      for (f <- links) v += getNormal(f)
      v.normalize()
      newNormals += new Vector3(v)
    }

    val newFaces = faces.map(f => new Face(f.vertices.map(v => new VertexData(v.v, v.vt, v.v))))

    new ObjFile(vertices, newNormals, uvs, newFaces, notParsed)
  }

  def removeNormals(): ObjFile = {
    val newFaces = faces.map(f => new Face(f.vertices.map(v => new VertexData(v.v, v.vt, -1))))
    new ObjFile(vertices, new ArrayBuffer[Vector3](), uvs, newFaces, notParsed)
  }

  def removeUVS(): ObjFile = {
    val newFaces = faces.map(f => new Face(f.vertices.map(v => new VertexData(v.v, -1, v.vn))))
    new ObjFile(vertices, normals, new ArrayBuffer[Vector2](), newFaces, notParsed)
  }

  def reorderVertices(): ObjFile = {
    val newFaces = faces.map(f => new Face(f.vertices.reverse))
    new ObjFile(vertices, normals, uvs, newFaces, notParsed)
  }

  def transform(mapVertices: Matrix4, mapNormals: Matrix4 = null): ObjFile = {
    val mapN = Option(mapNormals).getOrElse(mapVertices.cpy().inv().tra())
    new ObjFile(vertices.map(v => mapVertices * v), normals.map(n => mapN * n), uvs, faces, notParsed)
  }

  private def getNormal(f: Face): Vector3 = {
    if (hasNormals) {
      val r = new Vector3()
      for (v <- f.vertices) {
        r += normals(v.vn)
      }
      r.normalize()
      r
    } else {
      val r1 = vertices(f.vertices(0).v)
      val r2 = vertices(f.vertices(1).v)
      val r3 = vertices(f.vertices(2).v)
      val dr = r2 - r1
      val dr2 = r3 - r2
      dr.setCross(dr, dr2)
      new Vector3(dr)
    }
  }

  private def makeMeshWithIndices(): Mesh = {
    val numbers = new mutable.HashMap[VertexData, Int]()
    val arr = new ArrayBuffer[VertexData](vertices.size + 1)

    for (f <- faces) {
      for (v <- f.vertices) {
        numbers.applyOrElse(v, (v: VertexData) => {
          val pos = numbers.size
          numbers.put(v, pos)
          arr += v
          pos
        })
      }
    }

    Logging.objFilesLog("making mesh with indices, polygons count = %d, unique vertices = %d"
      .format(faces.size, arr.size))

    val floats = new FloatBuf(numbers.size * vertexSize)
    val indices = new ShortBuf(faces.size * 3)
    arr.foreach(v => putVertex(v, floats))
    faces.foreach(f => f.vertices.foreach(v => indices += numbers(v)))

    val mesh = new Mesh(true, arr.size, faces.size * 3, vertexAttributes: _*)
    mesh.setVertices(floats.getArray())
    mesh.setIndices(indices.getArray())

    mesh
  }

  private def putVertex(v: VertexData, to: FloatBuf): Unit = {
    to += vertices(v.v)
    if (v.vn >= 0) to += normals(v.vn)
    if (v.vt >= 0) to += uvs(v.vt)
  }

  private def vertexAttributes: Array[VertexAttribute] = {
    val lst = new ArrayBuffer[VertexAttribute](3)
    lst += new VertexAttribute(Usage.Position, 3, "aPos")
    if (hasNormals) lst += new VertexAttribute(Usage.Normal, 3, "aNormal")
    if (hasUVs) lst += new VertexAttribute(Usage.TextureCoordinates, 2, "aTexPos")
    lst.toArray
  }

  private def parse(raw: String, uvFlipY: Boolean): Unit = {
    def parseVec3(arr: Array[String]) = new Vector3(arr(1).toFloat, arr(2).toFloat, arr(3).toFloat)

    def flip(value: Float) = if (uvFlipY) 1f - value else value

    def parseVec2(arr: Array[String]) = new Vector2(arr(1).toFloat, flip(arr(2).toFloat))

    def parseVertexData(line: String) = {
      val arr = line.split("/")

      def get(i: Int) = if (arr.size > i && arr(i) != "") arr(i).toInt - 1 else -1

      new VertexData(get(0), get(1), get(2))
    }

    def parseFace(arr: Array[String]): Face = {
      val face = new Face(new Array[VertexData](arr.length - 1))
      for (i <- 1 until arr.length) face.vertices(i - 1) = parseVertexData(arr(i))
      face
    }

    def parseLine(line: String): Unit = {
      val t = line.trim
      if (t == "" || t.charAt(0) == '#') return
      val arr = t.split(" ")
      arr(0) match {
        case "v" => vertices += parseVec3(arr)
        case "vt" => uvs += parseVec2(arr)
        case "vn" => normals += parseVec3(arr)
        case "f" => faces += parseFace(arr)
        case _ => notParsed += line
      }
    }

    raw.split("\n").foreach(parseLine)
  }
}
