/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

/**
  * class for logging system
  *
  * Created by Igor Slobodskov on 30.07.2015.
  */
object Logging {

  def log(a: Any): String = {
    val s = a.toString
    System.out.println(s)
    s
  }

  def timeLog(a: Any): String = {
    log("t = " + FPSCounter.time.toString + " : " + a)
  }

  def shaderLog(msg: Any): String = {
    log("shaders : " + msg)
  }

  def objFilesLog(msg: Any): String = {
    log("objFile : " + msg)
  }

  def logFrameDelay(delta: Int): String = {
    val fps = FPSCounter.fps
    if (delta > 2000 / (fps + 1))
      timeLog("frame delay : " + delta + ", but average fps = " + FPSCounter.fps)
    else
      ""
  }
}
