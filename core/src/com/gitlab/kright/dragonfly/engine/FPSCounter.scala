/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

/**
  * libgdx has fpsCounter, but I want some custom features
  *
  * Created by Igor Slobodskov on 27.07.2015.
  */
object FPSCounter extends FPSCounter(System.currentTimeMillis())

class FPSCounter(val startTime: Long = System.currentTimeMillis()) {

  private var framesCount: Int = 0
  private var counter: Int = 0

  private var prev: Long = startTime
  private var now: Long = startTime
  private var delta: Int = 0

  private var fpsCount: Int = 0

  private def getTime: Long = System.currentTimeMillis()

  def update(): Int = {
    this.synchronized {
      framesCount += 1
      counter += 1

      val old = now
      now = getTime
      delta = (now - old).toInt
      if (now - prev > 1000) {
        fpsCount = 1 + (counter * 1000 - 1) / (now - prev).toInt
        counter = 0
        prev = now
      }
    }
    Logging.logFrameDelay(delta)

    fpsCount
  }

  def time: Long = now

  def fps: Int = fpsCount

  def deltaTime: Int = delta

  /**
    * @return total number of update() calls
    */
  def updatesCount: Int = framesCount
}
