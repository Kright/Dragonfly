/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.utils.Disposable

import scala.collection.mutable.ArrayBuffer

class Disposer {

  val elements = new ArrayBuffer[Disposable]()
  private var isValid: Boolean = true

  def apply[T <: Disposable](elem: T): T = {
    elements += elem
    elem
  }

  def dispose(): Unit = {
    assert(isValid)
    isValid = true
    elements.reverseIterator.foreach(_.dispose())
    elements.clear()
  }
}
