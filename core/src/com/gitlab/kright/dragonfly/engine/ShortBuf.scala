/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

/**
  * Helper classes for filling arrays
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class ShortBuf(val size: Int) {

  private val arr = new Array[Short](size)
  private var pos = 0

  def +=(v: Int): Unit = {
    arr(pos) = v.toShort
    pos += 1
  }

  def +=(v1: Int, v2: Int, v3: Int): Unit = {
    this += v1
    this += v2
    this += v3
  }

  def getArray(): Array[Short] = {
    assert(pos == size, "Buf error, position is %d, but size is %d".format(pos, size))
    arr
  }
}
