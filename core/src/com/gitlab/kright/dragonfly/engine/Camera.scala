/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.math.{Matrix4, Quaternion, Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.math._

/**
  * class for game camera
  *
  * Created by Igor Slobodskov on 19.07.2015.
  */
class Camera(val pos: Vector3, val orientation: Quaternion) {

  // todo rm gamma and exposure, make class more abstract

  var exposure: Float = 1.0f
  var gamma: Float = 2.2f

  val position = new Position(pos, orientation)

  def this() = this(new Vector3, new Quaternion())

  var far: Float = 10000f
  var near: Float = far * 0.0001f
  var fov: Float = 45
  var aspect: Float = 16f / 10

  val projection = new Matrix4
  val view = new Matrix4
  val viewProjection = new Matrix4

  val forward = new Vector3
  val right = new Vector3
  val up = new Vector3

  val rSize = new Vector2

  //init
  {
    setProjection(near, far, fov, aspect)
    update()
  }

  def setProjection(near: Float = near, far: Float = far, fov: Float = fov, aspect: Float = aspect): Unit = {
    this.far = far
    this.near = near
    this.fov = fov
    this.aspect = aspect
    projection.setToProjection(near, far, fov, aspect)

    val ry: Float = Math.tan((fov * (Math.PI / 180)) / 2.0).toFloat
    val rx: Float = aspect * ry
    rSize := (rx, ry)
  }

  def update(): Unit = {
    view.set(orientation.x, orientation.y, orientation.z, -orientation.w)
    view.translate(-pos.x, -pos.y, -pos.z)

    viewProjection.set(view)
    viewProjection.mulLeft(projection)

    orientation.getZ(forward)
    forward *= -1
    orientation.getX(right)
    orientation.getY(up)
  }

  def project(inOutWorldPos: Vector3): Vector3 =
    inOutWorldPos prj viewProjection
}
