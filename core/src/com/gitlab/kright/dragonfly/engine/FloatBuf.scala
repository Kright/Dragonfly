/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.math.{Vector2, Vector3}

/**
  * Helper classes for filling arrays
  *
  * Created by Igor Slobodskov on 30.07.2015.
  */
class FloatBuf(val size: Int) {

  private val arr = new Array[Float](size)
  private var pos = 0

  def +=(v: Float): Unit = {
    arr(pos) = v
    pos += 1
  }

  def +=(v1: Float, v2: Float, v3: Float): Unit = {
    this += v1
    this += v2
    this += v3
  }

  def +=(v: Vector3): Unit = {
    this += v.x
    this += v.y
    this += v.z
  }

  def +=(v: Vector2): Unit = {
    this += v.x
    this += v.y
  }

  def getArray(): Array[Float] = {
    assert(pos == size, "Buf error, position is %d, but size is %d".format(pos, size))
    arr
  }
}

