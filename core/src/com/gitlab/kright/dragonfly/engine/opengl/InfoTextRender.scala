/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.{BitmapFont, SpriteBatch}
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Camera


class InfoTextRender extends Disposable {

  private val bitmapFont = new BitmapFont()
  private val spriteBatch = new SpriteBatch()

  def render(color: Color, lines: String*): Unit = {
    val dh = bitmapFont.getLineHeight
    val h = Gdx.graphics.getHeight
    val x = Gdx.graphics.getWidth * 0.03f

    bitmapFont.setColor(color)
    spriteBatch.begin()
    for ((line, i) <- lines.zipWithIndex) {
      bitmapFont.draw(spriteBatch, line, x, h - dh * i)
    }
    spriteBatch.end()
  }

  def render(text: String, worldPos: Vector3, camera: Camera, color: Color = Color.WHITE): Unit = {
    val screenPos = camera.project(worldPos.cpy())
    if (screenPos.z < 1.0f) {
      val x = (1 + screenPos.x) * 0.5f * Gdx.graphics.getWidth
      val y = (1 + screenPos.y) * 0.5f * Gdx.graphics.getHeight
      bitmapFont.setColor(color)
      spriteBatch.begin()
      bitmapFont.draw(spriteBatch, text, x, y)
      spriteBatch.end()
    }
  }

  override def dispose(): Unit = {
    spriteBatch.dispose()
    bitmapFont.dispose()
  }
}
