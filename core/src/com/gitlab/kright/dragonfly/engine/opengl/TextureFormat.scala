/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.graphics.{GL20, GL30}

case class TextureFormat(toInt: Int)

object TextureFormat {
  val GL_RED: TextureFormat = TextureFormat(GL30.GL_RED)
  val GL_RG: TextureFormat = TextureFormat(GL30.GL_RG)
  val GL_RGB: TextureFormat = TextureFormat(GL20.GL_RGB)
  val GL_RGBA: TextureFormat = TextureFormat(GL20.GL_RGBA)

  val GL_LUMINANCE: TextureFormat = TextureFormat(GL20.GL_LUMINANCE)
  val GL_LUMINANCE_ALPHA: TextureFormat = TextureFormat(GL20.GL_LUMINANCE_ALPHA)
  val GL_ALPHA: TextureFormat = TextureFormat(GL20.GL_ALPHA)

  val GL_DEPTH_COMPONENT: TextureFormat = TextureFormat(GL20.GL_DEPTH_COMPONENT)
  val GL_DEPTH_STENCIL: TextureFormat = TextureFormat(GL30.GL_DEPTH_STENCIL)

  val GL_RED_INTEGER: TextureFormat = TextureFormat(GL30.GL_RED_INTEGER)
  val GL_RG_INTEGER: TextureFormat = TextureFormat(GL30.GL_RG_INTEGER)
  val GL_RGB_INTEGER: TextureFormat = TextureFormat(GL30.GL_RGB_INTEGER)
  val GL_RGBA_INTEGER: TextureFormat = TextureFormat(GL30.GL_RGBA_INTEGER)
}
