/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Disposable

class VertexArrayObjectGL extends BindableImpl with Disposable {

  val id: Int = Gdx.gl30.glGenVertexArray()
  bind()

  override protected def bind(i: Int): Unit = Gdx.gl30.glBindVertexArray(i)

  override def dispose(): Unit = {
    Gdx.gl30.glDeleteVertexArrays(1, Array(id), 0)
  }
}
