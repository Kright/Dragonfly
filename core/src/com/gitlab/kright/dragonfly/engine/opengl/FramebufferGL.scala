/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL20.{GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT, GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT, GL_FRAMEBUFFER_UNSUPPORTED, _}
import com.badlogic.gdx.graphics.GL30.{GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE, GL_FRAMEBUFFER_UNDEFINED}
import com.badlogic.gdx.utils.Disposable

class FramebufferGL extends BindableImpl with Disposable {

  val id: Int = Gdx.gl30.glGenFramebuffer()

  bind()


  def target: Int = GL20.GL_FRAMEBUFFER

  /**
    *
    * @param attachment    : one of the following: GL_COLOR_ATTACHMENTi, GL_DEPTH_ATTACHMENT, GL_STENCIL_ATTACHMENT,
    *                      GL_DEPTH_STENCIL_ATTACHMENT
    * @param textureTarget :
    * @param textureId     : may be 0 (no texture)
    * @param level         :
    */
  def glTexture2d(attachment: Int, textureTarget: Int, textureId: Int = 0, level: Int = 0): Unit = {
    Gdx.gl30.glFramebufferTexture2D(target, attachment, textureTarget, textureId, level)
  }

  def glRenderbuffer(attachment: Int, renderbufferTarget: Int, renderbufferId: Int): Unit = {
    assert(renderbufferTarget == GL20.GL_RENDERBUFFER)
    Gdx.gl30.glFramebufferRenderbuffer(target, attachment, renderbufferTarget, renderbufferId)
  }

  def assertIsComplete(): Unit = {
    val status = Gdx.gl30.glCheckFramebufferStatus(target)
    assert(status == GL_FRAMEBUFFER_COMPLETE, FramebufferGL.describeStatus(status))
  }

  def invalidate(): Unit = {
    // todo Gdx.gl30.glInvalidateFramebuffer(target, )
  }

  def delete(): Unit = {
    Gdx.gl30.glDeleteFramebuffer(id)
  }

  override protected def bind(bindId: Int): Unit = Gdx.gl30.glBindFramebuffer(GL_FRAMEBUFFER, bindId)

  override def dispose(): Unit = {
    Gdx.gl30.glDeleteFramebuffer(target)
  }
}

object FramebufferGL {

  def describeStatus(status: Int): String = {
    status match {
      case GL_FRAMEBUFFER_COMPLETE => "framebuffer complete"
      case GL_FRAMEBUFFER_UNDEFINED => "framebuffer undefined"
      case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT => "framebuffer incomplete attachment"
      case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT => "framebuffer incomplete missing attachment"
      case GL_FRAMEBUFFER_UNSUPPORTED => "framebuffer unsupported"
      case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE => "framebuffer incomplete multisample"
    }
  }
}
