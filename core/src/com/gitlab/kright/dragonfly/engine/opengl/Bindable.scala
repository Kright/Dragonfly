/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

// todo switch to abstract class and to allow compiler inline method
trait Bindable {

  def bind(): Unit

  def unbind(): Unit

  @inline
  final def binded(@inline func: => Unit): Unit = {
    bind()
    func
    unbind()
  }
}

trait BindableImpl extends Bindable {

  def id: Int

  protected def bind(i: Int): Unit


  @inline
  override def bind(): Unit = bind(id)

  @inline
  override def unbind(): Unit = bind(0)
}
