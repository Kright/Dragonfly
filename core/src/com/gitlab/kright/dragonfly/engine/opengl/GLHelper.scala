/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import java.nio.{ByteBuffer, ByteOrder, FloatBuffer, ShortBuffer}

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.profiling.GLInterceptor
import com.badlogic.gdx.math.{Matrix3, Matrix4, Vector3}

/**
  *
  * class with some helper functions
  *
  * Created by Igor Slobodskov on 19.07.2015.
  *
  * todo move to game engine
  */
object GLHelper {

  def makeBuffer(arr: Array[Float]): FloatBuffer = {
    val buf = makeFloatBuffer(arr.length)
    buf.put(arr)
    buf.position(0)
    buf
  }

  def makeFloatBuffer(floatsCount: Int): FloatBuffer =
    ByteBuffer.allocateDirect(floatsCount * 4).order(ByteOrder.nativeOrder()).asFloatBuffer()

  def makeBuffer(arr: Array[Short]): ShortBuffer = {
    val buf = makeShortBuffer(arr.length)
    buf.put(arr)
    buf.position(0)
    buf
  }

  def makeShortBuffer(shortsCount: Int): ShortBuffer =
    ByteBuffer.allocateDirect(shortsCount * 2).order(ByteOrder.nativeOrder()).asShortBuffer()

  def getErrorString(code: Int): String = GLInterceptor.resolveErrorNumber(code)

  def setMatrix(location: Int, m: Matrix4): Unit = Gdx.gl20.glUniformMatrix4fv(location, 1, false, m.getValues, 0)

  def setMatrix(location: Int, m: Matrix3): Unit = Gdx.gl20.glUniformMatrix3fv(location, 1, false, m.getValues, 0)

  def setUniform(location: Int, v: Vector3): Unit = Gdx.gl20.glUniform3f(location, v.x, v.y, v.z)

  def makeMatrix4FromTrace(tr0: Float, tr1: Float, tr2: Float, tr3: Float = 1): Matrix4 = {
    val m = new Matrix4()
    m.`val`(0) = tr0
    m.`val`(5) = tr1
    m.`val`(10) = tr2
    m.`val`(15) = tr3
    m
  }

  @inline
  def measureNanoTime(@inline code: => Unit): Long = {
    val start = System.nanoTime()
    code
    val end = System.nanoTime()
    end - start
  }

  def describeError(code: Int): String = {
    code match {
      case GL20.GL_NO_ERROR => "GL_NO_ERROR"
      case GL20.GL_INVALID_OPERATION => "GL_INVALID_OPERATION"
      case GL20.GL_INVALID_VALUE => "GL_INVALID_VALUE"
    }
  }

  def glCheckError(): Unit = {
    val error = Gdx.gl30.glGetError()
    assert(error == GL20.GL_NO_ERROR, s"error = ${describeError(error)}" )
  }
}
