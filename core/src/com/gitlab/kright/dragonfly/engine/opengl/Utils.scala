/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import java.nio.{Buffer, FloatBuffer, IntBuffer}
import com.badlogic.gdx.utils.BufferUtils

object Utils {
  def makeFloatBuffer(array: Array[Float]): FloatBuffer = {
    val buf = BufferUtils.newFloatBuffer(array.size)
    buf.put(array)
    // https://stackoverflow.com/questions/58120357/jeromq-on-android-no-virtual-method-clearljava-nio-bytebuffer
    buf.asInstanceOf[Buffer].position(0)
    buf
  }

  def makeIntBuffer(array: Array[Int]): IntBuffer = {
    val buf = BufferUtils.newIntBuffer(array.size)
    buf.put(array)
    buf.asInstanceOf[Buffer].position(0)
    buf
  }

  // https://stackoverflow.com/questions/28375338/cube-using-single-gl-triangle-strip
  def makeCubeTrianglesStrip(): Array[Float] =
    Array(
      -1f, 1f, 1f,     // Front-top-left
      1f, 1f, 1f,      // Front-top-right
      -1f, -1f, 1f,    // Front-bottom-left
      1f, -1f, 1f,     // Front-bottom-right
      1f, -1f, -1f,    // Back-bottom-right
      1f, 1f, 1f,      // Front-top-right
      1f, 1f, -1f,     // Back-top-right
      -1f, 1f, 1f,     // Front-top-left
      -1f, 1f, -1f,    // Back-top-left
      -1f, -1f, 1f,    // Front-bottom-left
      -1f, -1f, -1f,   // Back-bottom-left
      1f, -1f, -1f,    // Back-bottom-right
      -1f, 1f, -1f,    // Back-top-left
      1f, 1f, -1f      // Back-top-right
    )
}
