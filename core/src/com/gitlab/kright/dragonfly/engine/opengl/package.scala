/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.{GL30, Texture}
import scala.language.implicitConversions

package object opengl {

  type TextureWrap = Texture.TextureWrap
  type TextureFilter = Texture.TextureFilter

  implicit def internalFormatToInt(f: InternalFormat): Int = f.toInt

  implicit def textureFormatToInt(f: TextureFormat): Int = f.toInt

  implicit def textureTypeToInt(f: TextureType): Int = f.toInt

  implicit def textureWrapToInt(f: TextureWrap): Int = f.getGLEnum

  implicit def textureFilterToInt(f: TextureFilter): Int = f.getGLEnum

  implicit class Gl30Ext(val gl30: GL30) extends AnyVal {
    def glGenVertexArray(): Int = {
      val arr = Array(0)
      Gdx.gl30.glGenVertexArrays(1, arr, 0)
      arr(0)
    }
  }

}
