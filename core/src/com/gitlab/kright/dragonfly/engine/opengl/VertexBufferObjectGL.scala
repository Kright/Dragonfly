/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import java.nio.Buffer

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.Disposable

class VertexBufferObjectGL(val target: Int = GL20.GL_ARRAY_BUFFER) extends BindableImpl with Disposable {

  val id: Int = Gdx.gl30.glGenBuffer()
  bind()

  def setData(buffer: Buffer, size: Int, usage: Int = GL20.GL_STATIC_DRAW): Unit = {
    Gdx.gl30.glBufferData(target, size, buffer, usage)
  }

  def glEnableVertexAttribArray(indx: Int): Unit = Gdx.gl30.glEnableVertexAttribArray(indx)

  def glEnableVertexAttribPointer(indx: Int, valuesPerVertex: Int, `type`: Int = GL20.GL_FLOAT,
                                  normalized: Boolean = false, strideInBytes: Int = 0, offset: Int = 0): Unit =
    Gdx.gl30.glVertexAttribPointer(indx, valuesPerVertex, `type`, normalized, strideInBytes, offset)

  override protected def bind(i: Int): Unit = Gdx.gl30.glBindBuffer(target, i)

  override def dispose(): Unit = {
    Gdx.gl30.glDeleteBuffer(id)
  }
}
