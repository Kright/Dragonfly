/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.graphics.{GL20, GL30}

case class InternalFormat(toInt: Int)

/**
 * https://www.khronos.org/registry/OpenGL-Refpages/es3.0/html/glTexImage2D.xhtml
 */
object InternalFormat {
  // unsized
  val GL_RGBA: InternalFormat = InternalFormat(GL20.GL_RGBA)

  // sized
  val GL_RGBA8: InternalFormat = InternalFormat(GL30.GL_RGBA8)
  val GL_SRGB8_ALPHA8: InternalFormat = InternalFormat(GL30.GL_SRGB8_ALPHA8)
  val GL_RGB8: InternalFormat = InternalFormat(GL30.GL_RGB8)
  val GL_SRGB8: InternalFormat = InternalFormat(GL30.GL_SRGB8)

  // values from -128, to 127, may be used for normals storing, input should be signed byte
  val GL_RGB8_SNORM: InternalFormat = InternalFormat(GL30.GL_RGB8_SNORM)
  val GL_RGBA8_SNORM: InternalFormat = InternalFormat(GL30.GL_RGBA8_SNORM)

  // only two bits of alpha value
  val GL_RGB10_A2: InternalFormat = InternalFormat(GL30.GL_RGB10_A2)

  val GL_DEPTH_COMPONENT24: InternalFormat = InternalFormat(GL30.GL_DEPTH_COMPONENT24)
  val GL_DEPTH_COMPONENT16: InternalFormat = InternalFormat(GL20.GL_DEPTH_COMPONENT16)

  // float formats

  val GL_R16F: InternalFormat = InternalFormat(GL30.GL_R16F)
  val GL_RG16F: InternalFormat = InternalFormat(GL30.GL_RG16F)
  val GL_RGB16F: InternalFormat = InternalFormat(GL30.GL_RGB16F)

  val GL_R11F_G11F_B10F: InternalFormat = InternalFormat(GL30.GL_R11F_G11F_B10F)

  val GL_RGBA32: InternalFormat = InternalFormat(GL30.GL_RGBA32F)

  // todo complete list
}