/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Disposable

import scala.annotation.tailrec
import scala.util.{Failure, Success, Try}

class ShaderHotReloader(shaderName: String) extends Disposable {

  private val watchers =
    Seq(s"shaders/${shaderName}_vert.glsl", s"shaders/${shaderName}_frag.glsl").map(new FileWatcher(_))

  private var shader: Shader = load()

  @tailrec
  private def load(): Shader =
    Try {
      Shader.loadFromAssets(shaderName)
    } match {
      case Success(shader) => shader
      case Failure(_) =>
        while (!filesWereChanged) {
          Thread.sleep(1)
        }
        load()
    }

  def filesWereChanged: Boolean = watchers.count(_.wasChanged()) > 0

  def apply(): Shader = {
    if (filesWereChanged) {
      shader.dispose()
      shader = load()
    }
    shader
  }

  override def dispose(): Unit = shader.dispose()
}

private class FileWatcher(fileName: String) {
  var lastCached: Long = getLastModified()

  def getLastModified(): Long = Gdx.files.internal(fileName).lastModified()

  def wasChanged(): Boolean = {
    val newDate = getLastModified()
    val isChanged = newDate != lastCached
    lastCached = newDate
    isChanged
  }
}
