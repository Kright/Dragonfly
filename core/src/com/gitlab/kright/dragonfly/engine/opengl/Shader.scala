/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.{Matrix3, Matrix4, Vector2, Vector3}
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.engine.Logging

import scala.language.dynamics

/**
  * ShaderProgram adapter
  *
  * Created by Igor Slobodskov on 02.07.2015.
  */
class Shader(val program: ShaderProgram) extends Disposable with Dynamic {

  def this(vertexShader: String, fragmentShader: String) = this(new ShaderProgram(vertexShader, fragmentShader))

  assert(program.isCompiled, Logging.shaderLog(s"program is not compiled:\n${program.getLog}vertices:\n${addLineNumbers(program.getVertexShaderSource)}\nfragments:\n${addLineNumbers(program.getFragmentShaderSource)}"))

  var failOnEmpty: Boolean = true

  private def addLineNumbers(code: String): String =
    code.
      split("\n")
      .zipWithIndex
      .map { case (line, no) => s"$no\t: $line" }.mkString("\n")

  @inline
  final def run(@inline func: => Unit): Unit = {
    //todo use bind, so no inline required
    program.bind()
    func
  }

  override def dispose(): Unit = program.dispose()

  def selectDynamic(name: String): Int = program.getUniformLocation(name)

  def updateDynamic(name: String)(value: Any): Unit = {
    val location = program.getUniformLocation(name)
    if (location != -1) {
      value match {
        case f: Float => Gdx.gl30.glUniform1f(location, f)
        case i: Int => Gdx.gl30.glUniform1i(location, i)
        case v2: Vector2 => Gdx.gl30.glUniform2f(location, v2.x, v2.y)
        case v3: Vector3 => GLHelper.setUniform(location, v3)
        case m4: Matrix4 => GLHelper.setMatrix(location, m4)
        case m3: Matrix3 => GLHelper.setMatrix(location, m3)
      }
    } else {
      if (failOnEmpty) {
        throw new RuntimeException(s"Unknown uniform: '$name'")
      }
    }
  }
}

object Shader {
  def loadFromAssets(vertexShaderName: String, fragmentShaderName: String): Shader =
    new Shader(Gdx.files.internal(vertexShaderName).readString(), Gdx.files.internal(fragmentShaderName).readString())

  def loadFromAssets(name: String): Shader =
    loadFromAssets(s"shaders/${name}_vert.glsl", s"shaders/${name}_frag.glsl")
}
