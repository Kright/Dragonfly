/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.graphics.{GL20, GL30}

case class TextureType(toInt: Int)

object TextureType {
  // type of incoming pixel data

  val GL_UNSIGNED_BYTE: TextureType = TextureType(GL20.GL_UNSIGNED_BYTE)
  val GL_BYTE: TextureType = TextureType(GL20.GL_BYTE)
  val GL_UNSIGNED_SHORT: TextureType = TextureType(GL20.GL_UNSIGNED_SHORT)
  val GL_UNSIGNED_INT: TextureType = TextureType(GL20.GL_UNSIGNED_INT)


  //todo compete list

  val GL_FLOAT: TextureType = TextureType(GL20.GL_FLOAT)
  val GL_HALF_FLOAT: TextureType = TextureType(GL30.GL_HALF_FLOAT)
}
