/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import java.nio.Buffer

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.utils.Disposable

class TextureGL extends Disposable with BindableImpl {

  val id: Int = Gdx.gl30.glGenTexture()

  bind()

  def target: Int = GL20.GL_TEXTURE_2D

  def glTexImage2d(level: Int = 0, internalFormat: InternalFormat, width: Int, height: Int, border: Int = 0,
                   format: TextureFormat, textureType: TextureType = TextureType.GL_BYTE, pixels: Buffer = null): Unit = {
    Gdx.gl30.glTexImage2D(target, level, internalFormat, width, height, border, format, textureType, pixels)
  }

  def update(paramName: Int, value: Int): Unit = Gdx.gl30.glTexParameteri(target, paramName, value)

  def setFilter(minFilter: TextureFilter, magFilter: TextureFilter): Unit = {
    this (GL20.GL_TEXTURE_MIN_FILTER) = minFilter
    this (GL20.GL_TEXTURE_MAG_FILTER) = magFilter
  }

  def setWrap(u: TextureWrap, v: TextureWrap): Unit = {
    this (GL20.GL_TEXTURE_WRAP_S) = u
    this (GL20.GL_TEXTURE_WRAP_T) = v
  }

  def activeTexture(pos: Int): Int = {
    assert(pos >= 0 && pos < 32)
    Gdx.gl30.glActiveTexture(GL20.GL_TEXTURE0 + pos)
    bind() // yep, it's ok
    pos
  }

  // may be used for framebuffer textures
  def generateMipmap(): Unit = Gdx.gl30.glGenerateMipmap(target)

  def dispose(): Unit = Gdx.gl30.glDeleteTexture(id)

  override protected def bind(i: Int): Unit = Gdx.gl30.glBindTexture(target, i)
}
