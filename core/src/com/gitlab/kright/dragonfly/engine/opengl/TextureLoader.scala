/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.opengl

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20.GL_TEXTURE_2D
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.glutils.FileTextureData
import com.badlogic.gdx.graphics.{GL30, Pixmap, Texture}

object TextureLoader {

  def apply(name: String, genMipMaps: Boolean = true,
            wrap: Texture.TextureWrap = Texture.TextureWrap.Repeat,
            filterMin: Texture.TextureFilter = Texture.TextureFilter.MipMapLinearLinear,
            filterMag: Texture.TextureFilter = Texture.TextureFilter.Nearest,
            useSRGB: Boolean = false): Texture = {

    val file = Gdx.files.internal(name)

    val pixmap = new Pixmap(file) {
      override def getGLInternalFormat: Int = {
        if (useSRGB) {
          getFormat match {
            case Format.RGB888 => GL30.GL_SRGB8
            case Format.RGBA8888 => GL30.GL_SRGB8_ALPHA8
            case anyOther => throw new RuntimeException(s"sRGB for $anyOther isn't supported!!")
          }
        } else {
          super.getGLInternalFormat
        }
      }
    }

    val textureData = new FileTextureData(file, pixmap, null, false)
    val texture = new Texture(textureData)

    texture.setWrap(wrap, wrap)
    texture.setFilter(filterMin, filterMag)
    if (genMipMaps) Gdx.gl.glGenerateMipmap(GL_TEXTURE_2D)

    texture
  }
}
