/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine

import com.badlogic.gdx.math._

package object math {

  implicit class VectorExt[V <: com.badlogic.gdx.math.Vector[V]](val v: V) extends AnyVal {

    def :=(r: V): Unit = v.set(r)

    def :=(r: ConstView[V]): Unit = r.getValue(v)


    def +=(r: V): V = v.add(r)

    def -=(r: V): V = v.sub(r)

    def *=(mul: Float): V = v.scl(mul)

    def /=(div: Float): V = v.scl(1f / div)

    def madd(r: V, f: Float): V = v.mulAdd(r, f)


    def +(r: V): V = v.cpy().add(r)

    def -(r: V): V = v.cpy().sub(r)

    def *(mul: Float): V = v.cpy().scl(mul)

    def /(div: Float): V = v.cpy().scl(1f / div)

    def unary_- : V = v * -1


    def normalize(): V = v.nor()

    def length: Float = v.len()

    def length2: Float = v.len2()

    def dot(r: V): Float = v.dot(r)

    def copy(): V = v.cpy()

    def constView(): ConstView[V] = new ConstView[V] {
      override def getValue(result: V): V = result.set(v)

      override def makeCopy(): V = copy()
    }
  }


  implicit class Vector3Ext[T <: Vector3](val v: T) extends AnyVal {
    def :=(x: Float, y: Float, z: Float): Vector3 = v.set(x, y, z)

    def setCross(l: Vector3, r: Vector3): Unit = v.set(
      l.y * r.z - l.z * r.y,
      l.z * r.x - l.x * r.z,
      l.x * r.y - l.y * r.x)
  }


  implicit class Vector2Ext[T <: Vector2](val v: T) extends AnyVal {
    def :=(x: Float, y: Float): Vector2 = v.set(x, y)

    def cross(r: Vector2): Float = v.x * r.y - v.y * r.x
  }


  implicit class QuaternionExt(val q: Quaternion) extends AnyVal {
    def :=(w: Float, x: Float, y: Float, z: Float): Unit = q.set(x, y, z, w)

    def :=(r: Quaternion): Unit = q.set(r)

    def :=(axis: Vector3, angle: Float): Unit = {
      val a = angle / 2
      val len = axis.len
      if (len > 0.0001f) {
        val m = Math.sin(a).toFloat / len
        this := (Math.cos(a).toFloat, axis.x * m, axis.y * m, axis.z * m)
      } else {
        this := (1, 0, 0, 0)
      }
    }

    def :=(log: Vector3): Unit = {
      val len = log.len
      if (len > 0.0001f) {
        val a = len / 2
        val m = Math.sin(a).toFloat / len
        this := (Math.cos(a).toFloat, log.x * m, log.y * m, log.z * m)
      } else
        this := (1, 0, 0, 0)
    }

    def :=(v: ConstView[Quaternion]): Unit = v.getValue(q)


    def *>>(v: Vector3): Vector3 = v.mul(q)

    def *>>(r: Quaternion): Quaternion = r.mulLeft(q)

    def *=(r: Quaternion): Quaternion = q.mul(r)

    def ^=(pow: Float): Quaternion = q.exp(pow)

    def *(r: Quaternion): Quaternion = q.cpy().mul(r)

    def *(v: Vector3): Vector3 = q.transform(v.copy())

    def normalize(): Unit = q.nor()


    def log(result: Vector3 = new Vector3()): Vector3 = {
      val xyz = q.lengthXYZ
      if (xyz > 0.0001f) {
        val m = (Math.acos(q.w).toFloat * 2f) / xyz
        result.set(m * q.x, m * q.y, m * q.z)
      } else {
        result.set(0f, 0f, 0f)
      }
      result
    }


    def setInterpolate(p1: Quaternion, t1: Float, p2: Quaternion, t2: Float): Unit = {
      this :=
        (p1.w * t1 + p2.w * t2,
          p1.x * t1 + p2.x * t2,
          p1.y * t1 + p2.y * t2,
          p1.z * t1 + p2.z * t2)
    }


    def setLerp(p: Quaternion, q: Quaternion, t: Float): Unit = {
      if ((p dot q) > 0)
        setInterpolate(p, 1 - t, q, t)
      else
        setInterpolate(p, t - 1, q, t)

      normalize()
    }


    def setSlerp(p: Quaternion, q: Quaternion, t: Float): Unit = {
      if (t <= 0) {
        this := p
        return
      }
      if (t >= 1) {
        this := q
        return
      }

      var dot = p dot q
      val sign = if (dot > 0) {
        1
      } else {
        dot = -dot
        -1
      }

      if (dot * sign > 0.95) {
        this.setInterpolate(p, (1 - t) * sign, q, t)
        this.normalize()
      } else {
        val sin = Math.sqrt(1 - dot * dot).toFloat
        val mul = 1 / sin

        val omega = Math.atan2(sin, dot).toFloat

        val k0 = Math.sin(omega * (1 - t)).toFloat * mul
        val k1 = Math.sin(omega * t).toFloat * mul

        this.setInterpolate(p, k0 * sign, q, k1)
      }
    }


    def getX(result: Vector3 = new Vector3()): Vector3 = result.set(
      1f - 2f * (q.y * q.y + q.z * q.z), 2f * (q.x * q.y + q.w * q.z), 2f * (q.x * q.z - q.w * q.y))

    def getY(result: Vector3 = new Vector3()): Vector3 = result.set(
      2f * (q.x * q.y - q.w * q.z), 1f - 2f * (q.x * q.x + q.z * q.z), 2f * (q.y * q.z + q.w * q.x))

    def getZ(result: Vector3 = new Vector3()): Vector3 = result.set(
      2f * (q.x * q.z + q.w * q.y), 2f * (q.y * q.z - q.w * q.x), 1 - 2f * (q.x * q.x + q.y * q.y))


    def length: Float = q.len()

    def length2: Float = q.len2()

    def lengthXYZ: Float = Math.sqrt(q.x * q.x + q.y * q.y + q.z * q.z).toFloat

    def rotAngle: Float = 2f * Math.acos(q.w).toFloat

    def constView(): ConstView[Quaternion] = new ConstView[Quaternion] {
      override def getValue(result: Quaternion): Quaternion = result.set(q)

      override def makeCopy(): Quaternion = q.cpy()
    }
  }


  implicit class Matrix3Ext(val m: Matrix3) extends AnyVal {
    def *=(r: Matrix3): Matrix3 = m.mul(r)

    def *>>(r: Matrix3): Matrix3 = r.mulLeft(m)

    def :=(r: Matrix3): Matrix3 = m.set(r)

    def update(pos: Int, value: Float): Unit = m.`val`(pos) = value

    def update(x: Int, y: Int, value: Float): Unit = m.`val`(y * 3 + x) = value

    def apply(pos: Int): Float = m.`val`(pos)

    def apply(x: Int, y: Int): Float = m.`val`(y * 3 + x)
  }


  implicit class Matrix4Ext(val m: Matrix4) extends AnyVal {
    def *=(r: Matrix4): Matrix4 = m.mul(r)

    def *>>(r: Matrix4): Matrix4 = r.mulLeft(m)

    def *>>(v: Vector3): Vector3 = v.prj(m)

    def *(v: Vector3): Vector3 = v.cpy().prj(m)

    def :=(r: Matrix4): Matrix4 = m.set(r)

    def :=(q: Quaternion): Matrix4 = m.set(q)

    def :=(p: Position): Matrix4 = m.idt().translate(p.pos).rotate(p.rot)

    def update(pos: Int, value: Float): Unit = m.`val`(pos) = value

    def update(x: Int, y: Int, value: Float): Unit = m.`val`(y * 4 + x) = value

    def apply(pos: Int): Float = m.`val`(pos)

    def apply(x: Int, y: Int): Float = m.`val`(y * 4 + x)
  }

  @inline
  def clamp(value: Float, min: Float, max: Float): Float = scala.math.min(max, scala.math.max(value, min))

  @inline
  def clamp(value: Double, min: Double, max: Double): Double = scala.math.min(max, scala.math.max(value, min))
}
