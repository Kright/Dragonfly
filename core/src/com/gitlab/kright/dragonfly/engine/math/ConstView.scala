/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.math

trait ConstView[T] {
  def getValue(result: T): T

  def makeCopy(): T
}
