/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.math

import com.badlogic.gdx.math.{Quaternion, Vector3}

/**
  * derivative of the Position
  * may be used as representation of speed, acceleration or force
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class Derivative(val linear: Vector3, val angular: Vector3) {

  def this() = this(new Vector3, new Vector3)

  def :=(example: Derivative): Unit = {
    linear := example.linear
    angular := example.angular
  }

  private val qTemp = new Quaternion()

  def :=(first: Position, second: Position, deltaTime: Float): Unit = {
    assert(deltaTime > 0f)
    linear := second.pos
    linear -= first.pos
    linear /= deltaTime

    qTemp := first.rot
    qTemp.conjugate()
    qTemp.mulLeft(second.rot)

    qTemp.log(angular)
    angular /= deltaTime
  }
}
