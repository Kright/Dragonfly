/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.math

trait Point2i {
  def x: Int

  def y: Int

  def ==(r: Point2i): Boolean = x == r.x && y == r.y

  def toConst: ConstPoint2i
}

class MutPoint2i(var x: Int, var y: Int) extends Point2i {

  def this() = this(0, 0)

  def :=(xx: Int, yy: Int): Unit = {
    x = xx
    y = yy
  }

  override def toConst: ConstPoint2i = new ConstPoint2i(x, y)
}

class ConstPoint2i(val x: Int, val y: Int) extends Point2i {
  override def toConst: ConstPoint2i = this
}
