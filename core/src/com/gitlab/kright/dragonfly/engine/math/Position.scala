/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.engine.math

import com.badlogic.gdx.math.{Quaternion, Vector3}

/**
  * full information about position:
  * it holds position of center and orientation
  *
  * Created by Igor Slobodskov on 13.08.2015.
  */
class Position(val pos: Vector3, val rot: Quaternion) {

  def this() = this(new Vector3, new Quaternion)

  def :=(example: Position): Unit = {
    pos := example.pos
    rot := example.rot
  }

  private val tempQ = new Quaternion()

  def +=(d: Derivative, multiplier: Float): Unit = {
    pos.madd(d.linear, multiplier)
    tempQ := (d.angular, d.angular.length * multiplier)
    rot *= tempQ
  }
}


