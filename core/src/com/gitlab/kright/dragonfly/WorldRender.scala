/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly

import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.utils.Disposable
import com.gitlab.kright.dragonfly.deferred.SkyRenderParams
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.interfaces.VisibleState

trait WorldRender extends Disposable {
  def render(camera: Camera, airplanes: Seq[VisibleState], skyRenderParams: SkyRenderParams, decalMatrix: Matrix4): Unit
}
