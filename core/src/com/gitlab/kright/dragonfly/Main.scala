/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly

import com.badlogic.gdx.ApplicationListener

/**
  * main class
  *
  * Created by Igor Slobodskov on 19.07.2015.
  */
class Main(platformSpecific: PlatformSpecific) extends ApplicationListener {
  PlatformSpecific.instance = platformSpecific

  private lazy val gameLoop: FlightCycle = new FlightCycle()

  private def log(msg: String): Unit = println(s"Main.$msg")

  override def render(): Unit = {
    gameLoop.update()
    // todo handle app reopening on android
  }

  override def create(): Unit = log("create()")

  override def dispose(): Unit = {
    log("dispose()")
    gameLoop.dispose()
  }

  override def resize(width: Int, height: Int): Unit = log(s"resize($width, $height)")

  override def pause(): Unit = log("pause()")

  override def resume(): Unit = log("resume()")
}
