/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.profiling.{GLErrorListener, GLProfiler}
import com.badlogic.gdx.math.{FloatCounter, Matrix4, Vector3}
import com.gitlab.kright.dragonfly.deferred.{GizmoBuilder, GizmoRender, SkyRenderParams, WorldRenderDeferred}
import com.gitlab.kright.dragonfly.engine.{Camera, FPSCounter}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.engine.opengl.{GLHelper, InfoTextRender}
import com.gitlab.kright.dragonfly.interfaces.{Player, VisibleState}
import com.gitlab.kright.dragonfly.playerio.{CleverInput, InputProcessor, PlaneInput}
import com.gitlab.kright.dragonfly.utils.{GameTime, LazyDisposable}
import com.gitlab.kright.dragonfly.world.I_16


/**
 * game loop here
 *
 * Created by Igor Slobodskov on 21.07.2015.
 */
class FlightCycle {

  private val camera = new Camera()
  camera.setProjection(aspect = Gdx.graphics.getWidth.toFloat / Gdx.graphics.getHeight)

  private val infoTextRender = new InfoTextRender()

  private val skyRenderParams = new SkyRenderParams()

  private val player = new Player("me", 0)
  private val airplaneModel = new I_16() //new AirplaneModelImpl()

  private var planeState: VisibleState = null

  private val inputProcessor: InputProcessor = new CleverInput()

  private val glProfiler = new GLProfiler(Gdx.graphics) {
    if (Config.debugGL) {
      setListener(GLErrorListener.THROWING_LISTENER)
    }
    enable()
  }

  private val performanceCounter = new FloatCounter(100)

  private val gameTime = new GameTime()
  private var isPaused = false
  private var gameSpeedPow2 = 0
  private var isFreeCamera = false

  private val avatar = {
    val s = new world.State(gameTime.totalMs)
    val speed = 70f

    s.position.pos := (0, 25, 0)
    s.position.rot := (1f, 0.05f, 0.0f, 0.0f)
    s.position.rot.normalize()

    s.speed.linear := (0, 0, -speed)
    s.speed.angular := (0, 0, 0)

    new world.PlayerAvatar(player, airplaneModel, s)
  }

  private val decalMatrix: Matrix4 = new Matrix4().translate(0, 24, 0).scale(5, 5, 5)

  private val worldRender = LazyDisposable {
    println(s"screen size = ${Gdx.graphics.getWidth}, ${Gdx.graphics.getHeight}")
    new WorldRenderDeferred(new ConstPoint2i(Gdx.graphics.getWidth, Gdx.graphics.getHeight))
  }

  private val gizmoLinesRenderPass = LazyDisposable(new GizmoRender())
  private val gizmoBuilder = new GizmoBuilder()

  private def updateCamera(state: VisibleState, isFreeCamera: Boolean): Unit = {
    if (isFreeCamera) {
      var shift = FPSCounter.deltaTime.toFloat / 1000 * 10
      if (Gdx.input.isKeyPressed(Keys.SHIFT_LEFT)) shift *= 10

      if (Gdx.input.isKeyPressed(Keys.W)) camera.pos -= camera.orientation.getZ() * shift
      if (Gdx.input.isKeyPressed(Keys.S)) camera.pos += camera.orientation.getZ() * shift

      if (Gdx.input.isKeyPressed(Keys.A)) camera.pos -= camera.orientation.getX() * shift
      if (Gdx.input.isKeyPressed(Keys.D)) camera.pos += camera.orientation.getX() * shift

      if (Gdx.input.isKeyPressed(Keys.Q)) camera.pos -= camera.orientation.getY() * shift
      if (Gdx.input.isKeyPressed(Keys.E)) camera.pos += camera.orientation.getY() * shift
    } else {
      val distance = 5f
      camera.pos := state.position.pos
      camera.pos += camera.orientation.getZ() * distance
      camera.pos += camera.orientation.getY() * (distance / 5)
    }
    camera.update()
  }

  private def processInput(): PlaneInput = {
    if (Gdx.input.isKeyPressed(Keys.ESCAPE)) System.exit(0)

    if (Gdx.input.isKeyJustPressed(Keys.C)) isFreeCamera = !isFreeCamera
    if (Gdx.input.isKeyJustPressed(Keys.NUM_0)) isPaused = !isPaused
    if (Gdx.input.isKeyJustPressed(Keys.MINUS)) gameSpeedPow2 -= 1
    if (Gdx.input.isKeyJustPressed(Keys.EQUALS)) gameSpeedPow2 += 1

    val userInput = inputProcessor.getInput(planeState, camera, gameTime)
    avatar.setInput(userInput)

    userInput
  }

  private def renderInfo(currentState: VisibleState, playerInput: PlaneInput): Unit = {
    infoTextRender.render(
      Color.WHITE,
      s"fps = ${FPSCounter.fps}, " + getProfilerInfo(),
      f"performance: avg = ${performanceCounter.mean.getMean}%.5f +-${performanceCounter.mean.standardDeviation()}%.5f, " +
        f"min = ${performanceCounter.min}%.5f, max = ${performanceCounter.max}%.5f",
      s"engine ${(playerInput.engineTrust * 100f).toInt}%",
      f"alt = ${currentState.position.pos.y}%.1f m",
      f"spd = ${currentState.speed.linear.length * 3.6f}%.1f km/h",
      if (isPaused) "paused" else f"game speed = ${math.pow(2, gameSpeedPow2)}"
    )
  }

  private def renderGizmo(): Unit =
    if (Config.drawGizmo) {
      {
        //        val proj = new Matrix4().setToProjection(1, 10, math.toDegrees(math.atan(0.5)).toFloat * 2 , 2).inv()
        //        val m = new Matrix4().translate(planeState.position.pos).rotate(planeState.position.rot)
        //        gizmoBuilder.addCube(new Vector3(0, 1, 0), m *= proj)
      }

      gizmoLinesRenderPass().render(camera, gizmoBuilder)
      gizmoBuilder.clear()

      infoTextRender.render("map center", new Vector3(), camera, Color.GREEN)
    }

  def update(): Unit = {
    FPSCounter.update()

    if (!isPaused) {
      val gameSpeed = math.pow(2, gameSpeedPow2)
      gameTime.shift((FPSCounter.deltaTime * gameSpeed).toLong)
    }

    val nanoDt = GLHelper.measureNanoTime {
      val playerInput = processInput()
      planeState = avatar.getState(gameTime.totalMs)
      updateCamera(planeState, isFreeCamera)
      skyRenderParams.update(planeState.position.pos, gameTime)

      val states = (0 until 16).map(i => avatar.getState(gameTime.totalMs - i * 200 - 200 * (math.pow(2, i).toLong - 1)))
      worldRender().render(camera, states, skyRenderParams, decalMatrix)
      renderGizmo()
      states.zipWithIndex.foreach{ case (state, i) =>
        infoTextRender.render(s"airplane $i", state.position.pos + state.position.rot.getY(), camera, Color.GREEN)
      }
      renderInfo(planeState, playerInput)
    }

    performanceCounter.put((nanoDt.toDouble / 1000000000.0).toFloat)
    //todo add disposing and reloading all state
    //todo try to use apitrace : https://github.com/libgdx/libgdx/wiki/Profiling#apitrace
  }

  def dispose(): Unit = {
    worldRender.dispose()
    gizmoLinesRenderPass.dispose()
    GLHelper.glCheckError()
  }

  private def getProfilerInfo(): String = {
    if (!Config.debugGL) {
      return ""
    }

    val result = s"calls: ${glProfiler.getCalls}, drawCalls: ${glProfiler.getDrawCalls}" +
      s", shaderSwitches: ${glProfiler.getShaderSwitches}, bindings: ${glProfiler.getTextureBindings}" +
      s", vertices: ${glProfiler.getVertexCount.average}"

    glProfiler.reset()
    result
  }
}
