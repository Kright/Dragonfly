/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.math.{Quaternion, Vector2, Vector3}
import com.gitlab.kright.dragonfly.engine.{Camera, FPSCounter}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.interfaces.VisibleState
import com.gitlab.kright.dragonfly.utils.GameTime

/**
  * simple input for desktop
  * w-s, a-d, q-e = pitch, roll, yaw
  * mouse for cam rotation
  *
  * Created by Igor Slobodskov on 28.07.2015.
  */
class DesktopInput(var mouseSensitivity: Vector2 = new Vector2(0.005f, 0.005f),
                   var camRotSensitivity: Float = 4f)
  extends InputProcessor {

  private def pressed(key: Int) = Gdx.input.isKeyPressed(key)

  private val input = new PlaneNaiveInput(0)
  private val mouse = new Vector2()
  private val screenSize = new Vector2()

  private val yaw = new Quaternion()
  private val pitch = new Quaternion()

  private var engine = 0.5f

  private val vY = new Vector3(0, 1, 0)
  private val vX = new Vector3(1, 0, 0)

  override def getInput(state: VisibleState, camera: Camera, gameTime: GameTime): PlaneInput = {
    input.clear(gameTime.totalMs)

    if (state == null) {
      return input
    }

    if (pressed(Keys.SHIFT_LEFT))
      engine += 0.01f
    if (pressed(Keys.CONTROL_LEFT))
      engine -= 0.01f

    if (engine < 0f) engine = 0f
    if (engine > 1f) engine = 1f
    input.engineTrust = engine

    if (pressed(Keys.A)) input.roll = -1f
    if (pressed(Keys.D)) input.roll = 1f
    if (pressed(Keys.W)) input.pitch = -1f
    if (pressed(Keys.S)) input.pitch = 1f
    if (pressed(Keys.E)) input.yaw = 1f
    if (pressed(Keys.Q)) input.yaw = -1f

    if (Gdx.input.isTouched) {
      screenSize := (Gdx.graphics.getWidth, Gdx.graphics.getHeight)
      mouse := (Gdx.input.getX, screenSize.y - Gdx.input.getY)
      mouse -= screenSize / 2

      yaw := (vY, -mouse.x * mouseSensitivity.x)
      pitch := (vX, mouse.y * mouseSensitivity.y)

      camera.orientation := state.position.rot * yaw * pitch
    } else {
      camera.orientation.setLerp(state.position.rot, camera.orientation, 1f - camRotSensitivity * 0.001f * FPSCounter.deltaTime)
    }

    input
  }
}
