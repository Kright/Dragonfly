/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

/**
  * input of airplane
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
class PlaneNaiveInput(var time: Long,
                      var yaw: Float,
                      var pitch: Float,
                      var roll: Float,
                      var engineTrust: Float
                     ) extends PlaneInput {

  def this(time: Long) = this(time, 0, 0, 0, 0)

  def clear(time: Long): Unit = {
    this.time = time
    yaw = 0
    pitch = 0
    roll = 0
    engineTrust = 0
  }

  def :=(planeInput: PlaneInput): Unit = {
    time = planeInput.time
    yaw = planeInput.yaw
    pitch = planeInput.pitch
    roll = planeInput.roll
    engineTrust = planeInput.engineTrust
  }

  def validate(): Unit = {
    def clamp(v: Float, min: Float, max: Float) = if (v < min) min else if (v > max) max else v

    def clamp11(v: Float): Float = clamp(v, -1, 1)

    yaw = clamp11(yaw)
    pitch = clamp11(pitch)
    roll = clamp11(roll)

    engineTrust = clamp(engineTrust, 0, 1)
  }
}
