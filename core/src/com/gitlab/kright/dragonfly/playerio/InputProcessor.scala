/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.interfaces.VisibleState
import com.gitlab.kright.dragonfly.utils.GameTime

/**
  * it process input form keyboard, mouse, touchscreen for managing airplane and cam orientation
  *
  * Created by Igor Slobodskov on 28.07.2015.
  */
trait InputProcessor {

  /**
    *
    * @param state  may be null
    * @param camera - camera
    * @return player input
    */
  def getInput(state: VisibleState, camera: Camera, gameTime: GameTime): PlaneInput
}
