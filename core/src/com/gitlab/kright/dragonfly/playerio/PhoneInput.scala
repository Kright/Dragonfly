/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.gitlab.kright.dragonfly.engine.{Camera, FPSCounter}
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.interfaces.VisibleState
import com.gitlab.kright.dragonfly.utils.GameTime

/**
  * simple input - single-touch, two axes - roll and pitch
  *
  * Created by Igor Slobodskov on 28.07.2015.
  */
class PhoneInput(val sensitivity: Vector2 = new Vector2(0.002f, 0.004f),
                 var camRotSensitivity: Float = 4f)
  extends InputProcessor {

  private val input = new PlaneNaiveInput(0)

  private var wasPressed = false
  private val touchStart = new Vector2()
  private val touch = new Vector2()

  override def getInput(state: VisibleState, camera: Camera, gameTime: GameTime): PlaneInput = {
    input.clear(gameTime.totalMs)
    input.engineTrust = 1f

    if (state == null) return input

    if (Gdx.input.isTouched) {
      touch := (Gdx.input.getX, Gdx.input.getY)

      if (wasPressed) {
        touch -= touchStart
        input.roll = touch.x * sensitivity.x
        input.pitch = touch.y * sensitivity.y
      } else {
        touchStart := touch
      }

      wasPressed = true
    } else {
      wasPressed = false
    }

    camera.orientation.setLerp(state.position.rot, camera.orientation, 1f - camRotSensitivity * 0.001f * FPSCounter.deltaTime)

    input.validate()

    input
  }
}
