/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.math.{Quaternion, Vector2, Vector3}
import com.badlogic.gdx.{Application, Gdx}
import com.gitlab.kright.dragonfly.PlatformSpecific
import com.gitlab.kright.dragonfly.engine.Camera
import com.gitlab.kright.dragonfly.engine.math._
import com.gitlab.kright.dragonfly.interfaces.VisibleState
import com.gitlab.kright.dragonfly.utils.GameTime

/**
  * User rotate camera and
  * airplane trying fly in the direction of player view
  *
  * Created by Igor Slobodskov on 30.07.2015.
  */
class CleverInput(mouseSensitivity: Vector2 = new Vector2(0.005f, 0.005f),
                  var camRotSensitivity: Float = 4f)
  extends InputProcessor {

  private def pressed(key: Int) = Gdx.input.isKeyPressed(key)

  private val input = new PlaneNaiveInput(0)
  private val mouse = new Vector2()
  private val prevMouse = new Vector2()
  private val screenSize = new Vector2()

  private var wasPressed = false

  private val yaw = new Quaternion()
  private val pitch = new Quaternion()

  private var engine = 0.9f
  private val goalOrientation = new Quaternion()

  private val vX = new Vector3(1, 0, 0)
  private val vY = new Vector3(0, 1, 0)

  private val isDesktop = Gdx.app.getType == Application.ApplicationType.Desktop
  private var hasGoal = !isDesktop

  override def getInput(state: VisibleState, camera: Camera, gameTime: GameTime): PlaneInput = {
    input.clear(gameTime.totalMs)

    if (state == null) {
      return input
    }

    if (pressed(Keys.SHIFT_LEFT))
      engine += 0.01f
    if (pressed(Keys.CONTROL_LEFT))
      engine -= 0.01f

    if (engine < 0f) engine = 0f
    if (engine > 1f) engine = 1f
    input.engineTrust = engine

    if (isDesktop || Gdx.input.isTouched) {
      prevMouse := mouse
      screenSize := (Gdx.graphics.getWidth, Gdx.graphics.getHeight)
      mouse := getCursorPos()

      if (wasPressed) {
        val forw = camera.forward
        val az = (Math.PI - Math.atan2(forw.x, forw.z)).toFloat

        val h = Math.atan2(forw.y, Math.sqrt(forw.x * forw.x + forw.z * forw.z)).toFloat
        val maxH = (Math.PI / 2 - 0.001).toFloat

        yaw := (vY, -((mouse.x - prevMouse.x) * mouseSensitivity.x + az))
        pitch := (vX, clamp((mouse.y - prevMouse.y) * mouseSensitivity.y + h, -maxH, maxH))

        camera.orientation := yaw * pitch
      } else {
        wasPressed = true
      }

      if (isDesktop) {
        val px = screenSize.x.toInt / 2
        val py = screenSize.y.toInt / 2
        mouse := (px, py)
        Gdx.input.setCursorCatched(true)
        Gdx.input.setCursorPosition(px, py)
      }
    } else {
      wasPressed = false
      //camera.orientation.setLerp(state.orientation, camera.orientation, 1f - camRotSensitivity * 0.001f * FPSCounter.deltaTime)
    }

    if (pressed(Keys.NUM_1)) hasGoal = true
    if (pressed(Keys.NUM_2)) hasGoal = false

    if (pressed(Keys.Z)) camera.fov = 22f
    else camera.fov = 45
    camera.setProjection()

    if (hasGoal) {
      if (!pressed(Keys.C)) {
        goalOrientation := camera.orientation
      }

      val goal = goalOrientation.getZ() * -1
      val dy = state.position.rot.getY() dot goal
      val dx = state.position.rot.getX() dot goal

      /*
      if (Math.abs(dx) < 0.1f) {
        input.yaw = 10 * dx
        if ((state.speed.angular dot goalOrientation.getY() * dx) > 0f) {
          input.yaw *= 0.5f
        }
      }
      */

      input.pitch = dy * 5f
      if ((state.speed.angular dot goalOrientation.getX()) * dy > 0f) {
        input.pitch *= 0.7f
      }

      input.roll = dx * 5f

      if ((state.speed.angular dot goalOrientation.getZ() * dx) < 0f) {
        input.roll *= 0.1f
      }

      val sign = state.position.rot.getY() dot goalOrientation.getX()
      input.roll -= sign * .01f / (dx * dx + dy * dy + .01f)

      /*
      */
    }

    if (pressed(Keys.A)) input.roll = -1f
    if (pressed(Keys.D)) input.roll = 1f
    if (pressed(Keys.W)) input.pitch = -1f
    if (pressed(Keys.S)) input.pitch = 1f
    if (pressed(Keys.E)) input.yaw = 1f
    if (pressed(Keys.Q)) input.yaw = -1f

    input.validate()

    input
  }

  private def getCursorPos(): Vector2 = {
    val pos = PlatformSpecific.instance.cursor
    pos.y = screenSize.y - pos.y
    pos
  }
}
