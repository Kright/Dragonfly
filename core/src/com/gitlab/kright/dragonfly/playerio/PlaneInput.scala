/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly.playerio

import com.gitlab.kright.dragonfly.interfaces.TimeStamp

/**
  * interface for airplane input
  *
  * Created by Igor Slobodskov on 14.08.2015.
  */
trait PlaneInput extends TimeStamp {

  def yaw: Float // right is positive

  def pitch: Float // up is positive

  def roll: Float // clockwise is positive

  def engineTrust: Float
}
