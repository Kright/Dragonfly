#version 300 es

precision highp float;

uniform mat4 uMVP;
uniform vec2 uCamRxRy;

in vec4 aPos;

out vec2 vTexturePos;
out vec2 vScreenXY;

void main(){
    vec4 pos = uMVP * aPos;
    vec2 sceenPos = pos.xy / pos.w;

    gl_Position = vec4(sceenPos.xy, 0.0, 1.0);
    vTexturePos = sceenPos * 0.5 + vec2(0.5, 0.5);
    vScreenXY = uCamRxRy * sceenPos;
}
