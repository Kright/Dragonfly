#version 300 es

uniform mat4 uMatrix;
uniform mat4 uModelView;
uniform vec3 uRight;
uniform vec3 uUp;

in vec3 aPos;
in vec2 aScale;
in vec2 aTexPos;

out vec2 vTexPos;
out vec3 vNormal;
out float vDepth;

void main() {
    vec3 pos = aPos + aScale.x * uRight + aScale.y * uUp;
    gl_Position = uMatrix * vec4(pos.xyz, 1.0);
    vTexPos = aTexPos;

    vec2 bonus = (vTexPos * 2.0 - 1.0) * 0.8;

    vNormal = cross(uUp, uRight) + bonus.x * uRight + bonus.y * uUp;
    vDepth = -(uModelView * vec4(aPos, 1.0)).z;
}
