#version 300 es

precision mediump float;

uniform sampler2D uTexture;

in vec3 vNormal;
in vec2 vTexPos;
in float vDepth;

layout (location = 0) out vec4 gAlbedoRoughness;
layout (location = 1) out vec4 gNormalSpec;
layout (location = 2) out float gLinearDepth;

void main(){
    gNormalSpec = vec4(0.5 + 0.5 * normalize(vNormal), 0.5);
    // todo rm hard-coded specular
    gAlbedoRoughness = vec4(texture(uTexture, vTexPos).rgb, 0.04);
    gLinearDepth = vDepth;
}