#version 300 es

precision mediump float;

uniform sampler2D uTexture;

in vec2 vTexPos;
in vec3 vNormal;
in float vDepth;

layout (location = 0) out vec4 gAlbedoRoughness;
layout (location = 1) out vec4 gNormalSpec;
layout (location = 2) out float gLinearDepth;

void main(){
    vec4 color = texture(uTexture, vTexPos);
    if (color.a < 0.5) discard;

    gAlbedoRoughness = vec4(color.rgb, 0.5);
    gNormalSpec = vec4(0.5 + 0.5 * normalize(vNormal), 0.04);
    gLinearDepth = vDepth;
}
