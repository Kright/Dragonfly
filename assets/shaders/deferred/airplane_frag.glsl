#version 300 es

precision mediump float;

in vec3 vNormal;
in float vDepth;

layout (location = 0) out vec4 gAlbedoRoughness;
layout (location = 1) out vec4 gNormalSpec;
layout (location = 2) out float gLinearDepth;

void main(){
    gNormalSpec = vec4(0.5 + 0.5 * normalize(vNormal), 0.04);
    gAlbedoRoughness = vec4(0.2, 0.4, 0.6, 0.8);
    gLinearDepth = vDepth;
}
