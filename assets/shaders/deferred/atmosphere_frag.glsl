#version 300 es

precision highp float;

uniform vec3 uCamPos;
uniform vec3 uSkyLight;

uniform vec3 uAirColorDensityKm;
uniform float uAirColorDensityHalfHeightKm;

uniform sampler2D gColorHDR;
uniform sampler2D gLinearDepth;

in vec2 vTexCoords;
in vec3 vPixDir;

out vec4 fragmentColor;

float atmosphereDensity(vec3 pos1, vec3 pos2){
    float km = 1000.0;
    pos1 /= km;
    pos2 /= km;

    float h1 = pos1.y;
    float h2 = pos2.y;
    float dh = h2 - h1;
    float dh2 = dh*dh;

    vec2 dxz = pos1.xz - pos2.xz;
    float dx2 = dot(dxz, dxz);

    float alpha = 1.0 / uAirColorDensityHalfHeightKm;

    if (dh2 < 0.1){
       return exp(-0.5 * (alpha * (h1 + h2))) * sqrt(dx2 + dh2);
    } else {
        return abs(exp(-h1 * alpha) - exp(-h2 * alpha)) * (sqrt(1.0 + dx2/dh2) * uAirColorDensityHalfHeightKm);
    }
}

void main() {
    vec3 colorHDR = texture(gColorHDR, vTexCoords).rgb;

    float linearDepth = texture(gLinearDepth, vTexCoords).r;
    vec3 worldPos = uCamPos + linearDepth * vPixDir;

    vec3 colorsPassingCoeff = exp(-atmosphereDensity(uCamPos, worldPos) * uAirColorDensityKm);
    colorHDR = mix(uSkyLight, colorHDR, colorsPassingCoeff);

    fragmentColor = vec4(colorHDR, 1.0);
}
