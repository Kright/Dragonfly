#version 300 es

precision mediump float;

uniform sampler2D gLinearDepth;

uniform float uSunDepth;
uniform vec3 uSunColor;

in vec2 vCoords;
in vec2 vTexCoords;

out vec4 fragmentColor;

void main(){
    if (vCoords.x * vCoords.x + vCoords.y * vCoords.y >= 1.0)
        discard;

    float linearDepth = texture(gLinearDepth, vTexCoords).r;
    if (uSunDepth > linearDepth)
        discard;

    vec3 outColor = uSunColor;
    fragmentColor = vec4(outColor, 1.0);
}