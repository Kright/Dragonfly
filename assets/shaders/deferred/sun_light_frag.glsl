#version 300 es

precision highp float;

uniform vec3 uSunPosition;
uniform vec3 uSunLight;
uniform vec3 uSkyLight;

uniform vec3 uCamPos;
uniform float gamma;

uniform vec3 uAirColorDensityKm;
uniform float uAirColorDensityHalfHeightKm;

uniform sampler2D gAlbedoRoughness;
uniform sampler2D gNormalSpec;
uniform sampler2D gLinearDepth;

in vec2 vTexCoords;
in vec3 vPixDir;

out vec4 fragmentColor;

// todo change file name

void main() {
    vec4 albedoRoughness = texture(gAlbedoRoughness, vTexCoords);
    float rougness = albedoRoughness.a;
    vec3 albedo = albedoRoughness.rgb;
    albedo = pow(albedo, vec3(gamma));  // gamma correction

    float linearDepth = texture(gLinearDepth, vTexCoords).r;
    vec3 worldPos = uCamPos + linearDepth * vPixDir;

    vec4 normalSpec = texture(gNormalSpec, vTexCoords);
    float specular = normalSpec.a;
    vec3 normal = normalSpec.xyz * 2.0 - 1.0;

    vec3 result = albedo * (uSkyLight + uSunLight * max(0.0, dot(uSunPosition, normal)));

    fragmentColor = vec4(result, 1.0);
}
