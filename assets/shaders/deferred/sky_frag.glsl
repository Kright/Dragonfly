#version 300 es

precision mediump float;

uniform float uSkyLinearDepth;

layout (location = 0) out vec4 gAlbedoRoughness;
layout (location = 1) out vec4 gNormalSpec;
layout (location = 2) out float gLinearDepth;

void main(){
    gNormalSpec = vec4(0.0);
    gAlbedoRoughness = vec4(0.0);
    gLinearDepth = uSkyLinearDepth;
}
