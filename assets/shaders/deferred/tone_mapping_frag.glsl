#version 300 es

precision mediump float;

uniform float gamma;
uniform float exposure;

uniform sampler2D uColorHDR;

in vec2 vTexCoords;

out vec4 fragmentColor;

void main() {
    vec3 color = texture(uColorHDR, vTexCoords).rgb;

    color = vec3(1.0) - exp(-color * exposure); // tone mapping
    color = pow(color, vec3(1.0 / gamma)); // gamma correction

    fragmentColor = vec4(color, 1.0);
}
