#version 300 es

uniform mat4 uViewPojection;
uniform vec3 uSunPosition;
uniform vec2 udSize;

layout (location = 0) in vec2 aPos;

out vec2 vCoords;
out vec2 vTexCoords;

void main(){
    vec4 pos = uViewPojection * vec4(uSunPosition, 1.0);
    vec4 screenPosition = vec4(pos.xy / pos.w, 1.0, 1.0);
    screenPosition.xy += aPos * udSize; // todo rm this cheating

    gl_Position = screenPosition;
    vCoords = aPos;
    vTexCoords = screenPosition.xy * 0.5 + 0.5;
}
