#version 300 es

uniform mat4 uMatrix;
uniform mat3 uNormalMatrix;
uniform vec2 uTexturePos;
uniform float uTextureScale;
uniform mat4 uModelView;

in vec3 aPos;
in vec3 aNormal;   // normal in the model-space

out vec3 vNormal;     // normal in the world-space
out vec2 vTexPos;
out float vDepth;

void main(){
    gl_Position = uMatrix * vec4(aPos, 1.0);
    vNormal = uNormalMatrix * aNormal;
    vTexPos = aPos.xz * uTextureScale + uTexturePos;
    vDepth = -(uModelView * vec4(aPos, 1.0)).z; // todo change this: this isn't effective
}
