#version 300 es

precision lowp float;

in vec3 vColor;

layout (location = 0) out vec4 gColor;

void main() {
    gColor = vec4(vColor.rgb, 1.0);
}
