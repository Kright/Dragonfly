#version 300 es

uniform float uSkyDepth;

layout (location = 0) in vec2 aPosition;

void main() {
    gl_Position = vec4(aPosition, uSkyDepth, 1.0);
}
