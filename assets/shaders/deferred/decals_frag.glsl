#version 300 es

precision mediump float;

uniform sampler2D uLinearDepthTexture;
uniform sampler2D uColorTexture;

uniform mat4 uModelViewInvBias;
uniform vec3 uCamPos;

in vec2 vTexturePos;
in vec2 vScreenXY;

layout (location = 0) out vec4 gAlbedoRoughness;

bool isOut(float v){
    return v >= 1.0 || v <= 0.0;
}

void main() {
    vec2 texturePos = vTexturePos;
    float depth = texture(uLinearDepthTexture, texturePos).r;
    vec4 modelViewPos = vec4(vScreenXY * depth, -depth, 1.0);
    vec3 localPos = (uModelViewInvBias * modelViewPos).xyz;

    if (isOut(localPos.x) || isOut(localPos.y) || isOut(localPos.z)) {
        discard;
    }

    vec2 texDx = dFdx(vTexturePos);
    vec2 texDy = dFdy(vTexturePos);

    vec4 color = textureGrad(uColorTexture, localPos.xz, texDx, texDy);
    if (color.a < 0.5) {
        discard;
    }

    gAlbedoRoughness = vec4(color.rgb, 0.8);
}
