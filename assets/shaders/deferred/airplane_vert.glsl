#version 300 es

uniform mat4 uMVP;
uniform mat4 uModel;
uniform mat4 uModelView;

in vec4 aPos;
in vec3 aNormal;

out vec3 vNormal;
out float vDepth;

void main(){
    gl_Position= uMVP * aPos;
    vNormal = (uModel * vec4(aNormal.xyz, 0.0)).xyz;
    vDepth = -(uModelView * aPos).z;
}



