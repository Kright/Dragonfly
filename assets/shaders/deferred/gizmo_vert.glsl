#version 300 es

precision highp float;

uniform mat4 uViewProjection;

in vec3 aPos;
in vec3 aColor;

out vec3 vColor;

void main() {
    gl_Position = uViewProjection * vec4(aPos.xyz, 1.0);
    vColor = aColor;
}
