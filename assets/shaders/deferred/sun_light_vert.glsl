#version 300 es

uniform vec2 uCamRxRy;

uniform vec3 uCamForward;
uniform vec3 uCamRight;
uniform vec3 uCamTop;

layout (location = 0) in vec2 aPosition;

out vec2 vTexCoords;
out vec3 vPixDir;

void main() {
    vTexCoords = aPosition * 0.5 + 0.5;
    gl_Position = vec4(aPosition, 0.0, 1.0);
    vPixDir = uCamForward + uCamRight * uCamRxRy.x * aPosition.x + uCamTop * uCamRxRy.y * aPosition.y;
}
