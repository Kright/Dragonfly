## Dragonfly

Prototype of game engine for open worlds with flight simulator. Deferred rendering on top of Open GL.
Libgdx + Scala, project should work on PC and Android.

Initially project was started and 2015. I worked on it from time to time, but it is far for finished state even now.

### How run

**PC:**
```./gradlew desktop:run```

**Android:**
```
./gradlew android:installDebug
./gradlew android:run
```

### Project structure

There are three modules. Most of the code is in `core`. Modules `desktop` and `android` contain a bit of launching and platform-specific code.

Resources (models, textures, shaders) are placed in `assets` directory.

Because of problems with Scala 3 on Android, project still uses scala 2.13 and targets java 8 
I hope I will switch to Scala 3 sometime.
