/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.badlogic.gdx.backends.lwjgl3;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.gitlab.kright.dragonfly.PlatformSpecific;
import org.lwjgl.glfw.GLFW;

public class GLFWPlatformSpecific implements PlatformSpecific {
    private final double[] x = new double[1];
    private final double[] y = new double[1];

    @Override
    public Vector2 cursor() {
        GLFW.glfwGetCursorPos(getWindow(), x, y);
        return new Vector2((float)x[0], (float)y[0]);
    }

    private long getWindow() {
        return ((DefaultLwjgl3Input)Gdx.input).window.getWindowHandle();
    }
}
