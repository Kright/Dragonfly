/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        load();
    }

    private void load(){
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.numSamples = 2;
        config.r = config.g = config.b = 8;
        config.depth = 16;
        config.useCompass = false;
        config.useAccelerometer = false;
        config.useGL30 = true;

        initialize(new Main(new AndroidPlatformSpecific()), config);
    }
}
