/*
 * Copyright © 2024 Igor Slobodskov
 */

package com.gitlab.kright.dragonfly;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

class AndroidPlatformSpecific implements PlatformSpecific {
    @Override
    public Vector2 cursor() {
        return new Vector2(Gdx.input.getX(), Gdx.input.getY());
    }
}